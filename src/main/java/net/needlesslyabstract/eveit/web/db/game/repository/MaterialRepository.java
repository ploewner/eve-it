package net.needlesslyabstract.eveit.web.db.game.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.needlesslyabstract.eveit.web.db.game.entity.ActivityID;
import net.needlesslyabstract.eveit.web.db.game.entity.Material;

@RepositoryRestResource(exported = false)
public interface MaterialRepository
		extends JpaRepository<Material, ActivityID> {
	
	public List<Material> findByActivityIDAndTypeID(Integer activityID, Integer typeID);
}
