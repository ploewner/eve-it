package net.needlesslyabstract.eveit.web.db.game.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class SkillID implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer typeID;
	
	private Integer activityID;
	
	private Type skillType;
	
}
