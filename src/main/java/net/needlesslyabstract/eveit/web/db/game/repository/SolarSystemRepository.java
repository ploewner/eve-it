package net.needlesslyabstract.eveit.web.db.game.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.needlesslyabstract.eveit.web.db.game.entity.SolarSystem;

@RepositoryRestResource(exported = false)
public interface SolarSystemRepository
		extends JpaRepository<SolarSystem, Integer> {
	
	public List<SolarSystem> findByNameContaining(String name);

}
