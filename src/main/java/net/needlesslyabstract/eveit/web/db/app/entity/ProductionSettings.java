package net.needlesslyabstract.eveit.web.db.app.entity;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@ToString
@Getter @Setter @NoArgsConstructor
// TODO Server-side validation for all fields
// (including whether the maps contain unneeded keys)
// in all entities where this is embedded? in a special service to do it?
public class ProductionSettings {
	
	/*
	 * Using boxed types here, because production requests may override
	 * (or not override) the default page settings. We use null on
	 * request settings to indicate that the page's value for that
	 * setting should be used (i.e. it's not overridden).
	 */
	
	@Column
	private Integer defaultMeLevel;
	
	@Column
	private Integer defaultTeLevel;
	
	@Column
	private Integer defaultSkillLevel;
	
	@Column
	private Integer decryptorID;
	
	@ElementCollection(fetch = FetchType.EAGER)
	private Map<Integer, Integer> bpID2MeLevel;
	
	@ElementCollection(fetch = FetchType.EAGER)
	private Map<Integer, Integer> bpID2TeLevel;
	
	@ElementCollection(fetch = FetchType.EAGER)
	private Map<Integer, Integer> skillID2Level;
	
	@Column
	private Integer buySystemID;
	
	@Column
	private Integer sellSystemID;
	
	@Column
	private Float salesTax;
	
	@Column
	private Float brokerFee;
	
	@Column
	private Integer productionSystemID;
	
	@Column
	private Float manufacturingCostIndex;
	
	@Column
	private Float copyingCostIndex;
	
	@Column
	private Float inventionCostIndex;
	
	@Column
	private Float facilityTax;
	
}
