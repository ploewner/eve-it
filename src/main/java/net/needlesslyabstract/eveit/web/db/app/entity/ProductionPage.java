package net.needlesslyabstract.eveit.web.db.app.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import net.needlesslyabstract.eveit.web.Ownable;

@Entity
@Table
@ToString
@EqualsAndHashCode(of = "id")
@Getter @Setter @NoArgsConstructor
public class ProductionPage implements Ownable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NonNull
	@JoinColumn(nullable = false)
	@ManyToOne(optional = false)
	private User owner;
	
	@NonNull
	@Column(nullable = false)
	private String name;
	
	@NonNull
	@OneToMany(mappedBy = "page", cascade = CascadeType.ALL)
	private Set<ProductionRequest> requests;
	
	@Embedded
	private ProductionSettings settings;

	@Override
	public boolean isOwnedBy(User user) {
		return getOwner().equals(user);
	}
	
	public ProductionPage mergeFrom(ProductionPage that) {
		setName(that.getName());
		setSettings(that.getSettings());
		return this;
	}

}
