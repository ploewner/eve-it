package net.needlesslyabstract.eveit.web.db.view;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class ProductionRecipe {
	
	private int activityID;
	
	private ProductionMaterial mainComponent;
	
	private Map<Integer, ProductionMaterial> materials;
	
	private Map<Integer, ProductionSkill> skills;
	
	private int producedPerRun;
	
	private Integer maxProductionLimit;

}
