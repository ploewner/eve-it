package net.needlesslyabstract.eveit.web.db.app.entity;

public interface ProductionPageView {
	
	Long getId();
	
	String getName();
	
	ProductionSettings getSettings();

}
