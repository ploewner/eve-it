package net.needlesslyabstract.eveit.web.db.view;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.needlesslyabstract.eveit.web.db.game.entity.Type;

@ToString
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class ProductionSkill {

	private Type type;
	
	private int level;
	
}
