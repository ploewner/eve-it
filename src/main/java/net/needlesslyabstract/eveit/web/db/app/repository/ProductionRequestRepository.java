package net.needlesslyabstract.eveit.web.db.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionRequest;

@RepositoryRestResource(exported = false)
public interface ProductionRequestRepository
		extends JpaRepository<ProductionRequest, Long> {

}
