package net.needlesslyabstract.eveit.web.db.game.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@IdClass(MaterialID.class)
@Table(name = "industryActivityMaterials")
@ToString
@Getter @Setter @NoArgsConstructor
public class Material {

	@Id
	private Integer activityID;
	
	@Id
	private Integer typeID;

	@Id
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "materialTypeID")
	private Type materialType;

	@Column
	private int quantity;
	
}
