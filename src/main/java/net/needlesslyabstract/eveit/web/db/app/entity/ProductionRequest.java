package net.needlesslyabstract.eveit.web.db.app.entity;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import net.needlesslyabstract.eveit.web.Ownable;

@Entity
@Table
@ToString
@EqualsAndHashCode(of = "id")
@Getter @Setter @NoArgsConstructor
public class ProductionRequest implements Ownable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NonNull
	@Column(nullable = false)
	private String name;
	
	@NonNull
	@JoinColumn(nullable = false)
	@ManyToOne(optional = false)
	private ProductionPage page;
	
	@NonNull
	@Column
	private Integer requestedType;
	
	@Column
	private int requestedAmount;
	
	@Embedded
	private ProductionSettings settings;
	
	@Override
	public boolean isOwnedBy(User user) {
		return page.isOwnedBy(user);
	}

	public ProductionRequest mergeFrom(ProductionRequest that) {
		setName(that.getName());
		setRequestedAmount(that.getRequestedAmount());
		setSettings(that.getSettings());
		return this;
	}
	
}
