package net.needlesslyabstract.eveit.web.db.game.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class MaterialID implements Serializable {
	private static final long serialVersionUID = -6149684573193821482L;

	private Integer typeID;
	
	private Integer activityID;
	
	private Type materialType;

}
