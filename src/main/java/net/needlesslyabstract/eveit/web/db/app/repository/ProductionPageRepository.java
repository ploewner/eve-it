package net.needlesslyabstract.eveit.web.db.app.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionPage;
import net.needlesslyabstract.eveit.web.db.app.entity.ProductionPageView;

@RepositoryRestResource(exported = false)
public interface ProductionPageRepository
		extends JpaRepository<ProductionPage, Long> {
	
	ProductionPageView findOneProjectedById(Long id);
	
	List<ProductionPageView> findProjectedByIdIn(Collection<Long> ids);
	
}
