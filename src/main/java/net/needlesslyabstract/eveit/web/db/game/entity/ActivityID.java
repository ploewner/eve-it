package net.needlesslyabstract.eveit.web.db.game.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class ActivityID implements Serializable {
	private static final long serialVersionUID = -3989925979543130102L;

	private Integer typeID;
	
	private Integer activityID;
	
}
