package net.needlesslyabstract.eveit.web.db.view;

import lombok.*;
import net.needlesslyabstract.eveit.web.db.app.entity.ProductionSettings;
import net.needlesslyabstract.eveit.web.db.game.entity.Type;

@ToString
@EqualsAndHashCode(of = "id")
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class ProductionRequestView {
	
	private Long id;
	
	private String name;
	
	private int requestedAmount;
	
	private Type type;
	
	private ProductionSettings settings;
	
}
