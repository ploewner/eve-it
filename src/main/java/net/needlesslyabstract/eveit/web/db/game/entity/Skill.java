package net.needlesslyabstract.eveit.web.db.game.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@IdClass(SkillID.class)
@Table(name = "industryActivitySkills")
@ToString
@Getter @Setter @NoArgsConstructor
public class Skill {
	
	@Id
	private Integer activityID;
	
	@Id
	private Integer typeID;
	
	@Id
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "skillID")
	private Type skillType;
	
	@Column
	private int level;

}
