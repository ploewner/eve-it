package net.needlesslyabstract.eveit.web.db.game.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.needlesslyabstract.eveit.web.db.game.entity.Type;

@RepositoryRestResource(exported = false)
public interface TypeRepository
		extends JpaRepository<Type, Integer> {

}
