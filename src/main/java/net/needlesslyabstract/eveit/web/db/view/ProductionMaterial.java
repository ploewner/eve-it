package net.needlesslyabstract.eveit.web.db.view;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.needlesslyabstract.eveit.web.db.game.entity.Type;

@ToString
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class ProductionMaterial {
	
	private Type type;
	
	private int rawAmount;
	
	private Map<Integer, ProductionRecipe> recipes;

}
