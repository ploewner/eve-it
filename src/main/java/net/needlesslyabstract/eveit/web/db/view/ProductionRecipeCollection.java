package net.needlesslyabstract.eveit.web.db.view;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.needlesslyabstract.eveit.web.db.game.entity.Type;

@ToString
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class ProductionRecipeCollection {
	
	private Map<Integer, ProductionRecipe> recipes;
	
	private Map<Integer, Type> allBlueprints;
	
	private Map<Integer, Type> allSkills;

}
