package net.needlesslyabstract.eveit.web.db.game.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "invTypes")
@ToString
@EqualsAndHashCode(of = "id")
@Getter @Setter @NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Type {
	
	@Id
	@Column(name = "typeID")
	private Integer id;
	
	@Column(name = "typeName")
	private String name;

}
