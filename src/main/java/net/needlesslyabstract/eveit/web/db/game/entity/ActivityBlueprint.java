package net.needlesslyabstract.eveit.web.db.game.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "industryBlueprints")
@ToString
@EqualsAndHashCode(of = { "typeID" })
@Getter @Setter @NoArgsConstructor
public class ActivityBlueprint {
	
	@Id
	private Integer typeID;
	
	@Column
	private Integer maxProductionLimit;

}
