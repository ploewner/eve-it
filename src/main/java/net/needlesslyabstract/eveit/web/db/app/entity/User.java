package net.needlesslyabstract.eveit.web.db.app.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table
@ToString
@EqualsAndHashCode(of = "id")
@Getter @Setter @RequiredArgsConstructor @NoArgsConstructor
public class User {

	// TODO is this automatically indexed by being an ID?
	// if not, make it so.
	@Id
	@NonNull
	private String id;

	@NonNull
	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
	private Set<ProductionPage> pages;
	
	public static User of(String id) {
		return new User(id, new HashSet<>());
	}
	
}
