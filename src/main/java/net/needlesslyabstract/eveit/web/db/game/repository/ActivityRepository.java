package net.needlesslyabstract.eveit.web.db.game.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.needlesslyabstract.eveit.web.db.game.entity.Activity;
import net.needlesslyabstract.eveit.web.db.game.entity.ActivityID;

@RepositoryRestResource(exported = false)
public interface ActivityRepository
		extends JpaRepository<Activity, ActivityID> {
	
	public List<Activity> findByProductId(int productId);

}
