package net.needlesslyabstract.eveit.web.db.app.repository;

import java.util.Optional;

import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.needlesslyabstract.eveit.web.db.app.entity.User;

@RepositoryRestResource(exported = false)
public interface UserRepository extends Repository<User, String> {
	
	Optional<User> findOne(String id);
	
	<S extends User> S save(S user);
	
}
