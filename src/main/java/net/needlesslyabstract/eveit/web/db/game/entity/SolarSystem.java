package net.needlesslyabstract.eveit.web.db.game.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "mapSolarSystems")
@ToString
@EqualsAndHashCode(of = "id")
@Getter @Setter @NoArgsConstructor
public class SolarSystem {
	
	@Id
	@Column(name = "solarSystemID")
	private Integer id;
	
	@Column(name = "solarSystemName")
	private String name;
	
	@Column(name = "regionID")
	private Integer regionId;

}
