package net.needlesslyabstract.eveit.web.db.game.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.needlesslyabstract.eveit.web.db.game.entity.Skill;
import net.needlesslyabstract.eveit.web.db.game.entity.SkillID;

@RepositoryRestResource(exported = false)
public interface SkillRepository
		extends JpaRepository<Skill, SkillID> {
	
	public List<Skill> findByActivityIDAndTypeID(Integer activityID, Integer typeID);

}
