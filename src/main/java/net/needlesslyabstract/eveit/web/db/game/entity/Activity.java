package net.needlesslyabstract.eveit.web.db.game.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@IdClass(ActivityID.class)
@Table(name = "industryActivity")
@SecondaryTable(name = "industryActivityProducts")
@ToString
@EqualsAndHashCode(of = { "activityID", "typeID" })
@Getter @Setter @NoArgsConstructor
public class Activity {

	@Id
	private Integer activityID;
	
	@Id
	private Integer typeID;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(table = "industryActivityProducts", name = "productTypeID")
	private Type product;
	
	@Column(table = "industryActivityProducts", name = "quantity")
	private Integer producedPerRun;
	
	
}
