package net.needlesslyabstract.eveit.web.db.game.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.needlesslyabstract.eveit.web.db.game.entity.ActivityBlueprint;

@RepositoryRestResource(exported = false)
public interface ActivityBlueprintRepository
		extends JpaRepository<ActivityBlueprint, Integer> {

}
