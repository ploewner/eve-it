package net.needlesslyabstract.eveit.web.db.game;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import net.needlesslyabstract.eveit.SQLiteDialect;

@Configuration
@EnableJpaRepositories(
		entityManagerFactoryRef = "gamedataEntityManagerFactory",
		transactionManagerRef = "gamedataTransactionManager"
)
@EnableTransactionManagement
// TODO best externalize all configuration properties
public class GamedataConfig {
	
	// TODO externalize this!
	private static final String DATABASE_URL = "jdbc:sqlite:test-db/static-data.db";

	@Bean
	PlatformTransactionManager gamedataTransactionManager() {
		return new JpaTransactionManager(gamedataEntityManagerFactory().getObject());
	}
	
	@Bean
	LocalContainerEntityManagerFactoryBean gamedataEntityManagerFactory() {
		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		
		jpaVendorAdapter.setShowSql(true);
		jpaVendorAdapter.setGenerateDdl(false);
		jpaVendorAdapter.setDatabasePlatform(SQLiteDialect.class.getName());
		
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		
		factoryBean.setDataSource(gamedataDataSource());
		factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		factoryBean.setPackagesToScan(GamedataConfig.class.getPackage().getName());
		
		return factoryBean;
	}
	
	@Bean
	DataSource gamedataDataSource() {
		return DataSourceBuilder.create()
				.url(DATABASE_URL)
				.build();
	}
	
	
}
