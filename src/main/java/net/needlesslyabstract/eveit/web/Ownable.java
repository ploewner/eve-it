package net.needlesslyabstract.eveit.web;

import net.needlesslyabstract.eveit.web.db.app.entity.User;

public interface Ownable {
	
	public boolean isOwnedBy(User user);

}
