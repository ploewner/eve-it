package net.needlesslyabstract.eveit.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.filter.KeycloakAuthenticationProcessingFilter;
import org.keycloak.adapters.springsecurity.filter.KeycloakPreAuthActionsFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.preauth.x509.X509AuthenticationFilter;
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.stereotype.Component;

//@EnableZuulProxy
@SpringBootApplication(exclude = {
		DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class
})
public class EveItApplication {
	
//	public static final String WEBAPP_PACKAGE = "net.needlesslyabstract.eveit.web";
//	public static final String DS_APP_PACKAGE = WEBAPP_PACKAGE + ".db.app";
//	public static final String DS_GAME_PACKAGE = WEBAPP_PACKAGE + ".db.game";

	public static void main(String... args) {
		SpringApplication.run(EveItApplication.class, args);
	}
	
	/*
	 * =========================================================================
	 * ======================== Security Configuration ========================
	 * =========================================================================
	 */

	@Configuration
	@EnableWebSecurity
	public class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

		@Autowired
		public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
			auth.authenticationProvider(keycloakAuthenticationProvider());
		}

		@Bean
		@Override
		protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
			return new NullAuthenticatedSessionStrategy();
		}

		@Bean
		public FilterRegistrationBean keycloakAuthenticationProcessingFilterRegistrationBean(
				KeycloakAuthenticationProcessingFilter filter) {
			FilterRegistrationBean registrationBean = new FilterRegistrationBean(filter);
			registrationBean.setEnabled(false);
			return registrationBean;
		}

		@Bean
		public FilterRegistrationBean keycloakPreAuthActionsFilterRegistrationBean(
				KeycloakPreAuthActionsFilter filter) {
			FilterRegistrationBean registrationBean = new FilterRegistrationBean(filter);
			registrationBean.setEnabled(false);
			return registrationBean;
		}
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
					.sessionAuthenticationStrategy(sessionAuthenticationStrategy())
					.and()
				.addFilterBefore(keycloakPreAuthActionsFilter(), LogoutFilter.class)
				.addFilterBefore(keycloakAuthenticationProcessingFilter(), X509AuthenticationFilter.class)
				.exceptionHandling()
					.authenticationEntryPoint(authenticationEntryPoint())
					.and()
				.authorizeRequests()
					.antMatchers(HttpMethod.GET, "/", "/index.html", "/css/**", "/data/**", "/js/**").permitAll()
					.anyRequest().fullyAuthenticated()
//					.anyRequest().permitAll()
					.and()
				.csrf()
					.csrfTokenRepository(csrfTokenRepository());
		}

		private CsrfTokenRepository csrfTokenRepository() {
			return CookieCsrfTokenRepository.withHttpOnlyFalse();
		}

	}
	
	/*
	 * Allow cross-site requests to the keycloak server
	 */
	@Component
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public class CorsFilter implements Filter {
		
		@Value("${keycloak.url}")
		private String keycloakUrl;

		@Override
		public void doFilter(ServletRequest rq, ServletResponse rs, FilterChain chain)
				throws IOException, ServletException {
			HttpServletResponse response = (HttpServletResponse) rs;
			response.setHeader("Access-Control-Allow-Origin", keycloakUrl);
			response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
			response.setHeader("Access-Control-Allow-Headers", "x-auth-token, x-requested-with");
			response.setHeader("Access-Control-Max-Age", "3600");
			if (!((HttpServletRequest) rq).getMethod().equals("OPTIONS")) {
				chain.doFilter(rq, rs);
			}
		}

		@Override
		public void init(FilterConfig filterConfig) throws ServletException {
		}

		@Override
		public void destroy() {
		}

	}
	
}