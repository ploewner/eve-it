package net.needlesslyabstract.eveit.web.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import net.needlesslyabstract.eveit.web.service.ViewConverterService;

public abstract class AbstractController<D, V> {
	
	@Autowired
	ViewConverterService<D, V> toView;
	
	<T> ResponseEntity<T> viewToResponseEntity(Optional<T> view) {
		return view.map(ResponseEntity::ok)
				.orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	ResponseEntity<V> dataToResponseEntity(Optional<D> data) {
		return viewToResponseEntity(data.map(toView::convert));
	}
	
}
