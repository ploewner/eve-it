package net.needlesslyabstract.eveit.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.needlesslyabstract.eveit.web.db.view.ProductionRecipeCollection;
import net.needlesslyabstract.eveit.web.service.ProductionRecipeService;

@RestController
@RequestMapping("/productionRecipes")
public class ProductionRecipeController {
	
	@Autowired
	private ProductionRecipeService recipes;
	
	@GetMapping("/{productTypeId}")
	@Transactional("gamedataTransactionManager")
	public ProductionRecipeCollection get(@PathVariable("productTypeId") int productTypeId) {
		return recipes.get(productTypeId);
	}

}
