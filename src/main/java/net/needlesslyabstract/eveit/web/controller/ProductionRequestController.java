package net.needlesslyabstract.eveit.web.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionRequest;
import net.needlesslyabstract.eveit.web.db.view.ProductionRequestView;
import net.needlesslyabstract.eveit.web.service.ProductionRequestService;

@RestController
@RequestMapping("/productionRequests")
public class ProductionRequestController
		extends AbstractController<ProductionRequest, ProductionRequestView> {
	
	@Autowired
	ProductionRequestService requests;
	
	// Needs to be transactional so we can lazily serialize the requests
	@GetMapping("/list/{pageId}")
	@Transactional
	public ResponseEntity<Collection<ProductionRequestView>> list(@PathVariable("pageId") Long pageId) {
		return viewToResponseEntity(requests.listForPage(pageId).map(toView::convert));
	}
	
	@PostMapping("/{pageId}")
	public ResponseEntity<ProductionRequestView> create(@PathVariable("pageId") Long pageId, @RequestBody ProductionRequest request) {
		return dataToResponseEntity(requests.create(pageId, request));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductionRequestView> get(@PathVariable("id") Long id) {
		return dataToResponseEntity(requests.get(id));
	}
	
	@PatchMapping("/{id}")
	public ResponseEntity<ProductionRequestView> update(@PathVariable("id") Long id, @RequestBody ProductionRequest request) {
		return dataToResponseEntity(requests.update(id, request));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		return requests.delete(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
	}
	
}
