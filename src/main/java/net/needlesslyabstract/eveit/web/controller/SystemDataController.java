package net.needlesslyabstract.eveit.web.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.needlesslyabstract.eveit.web.db.game.entity.SolarSystem;
import net.needlesslyabstract.eveit.web.service.SolarSystemService;

@RestController
@RequestMapping("/systems")
public class SystemDataController {
	
	@Autowired
	SolarSystemService systems;
	
	@GetMapping("/find/{name}")
	public List<SolarSystem> find(@PathVariable("name") String name) {
		if(name.length() < 3) return Collections.emptyList();
		return systems.find(name);
	}
	
	@GetMapping("/{id}")
	public SolarSystem get(@PathVariable("id") int id) {
		return systems.get(id);
	}

}
