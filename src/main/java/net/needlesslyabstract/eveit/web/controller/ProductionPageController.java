package net.needlesslyabstract.eveit.web.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionPage;
import net.needlesslyabstract.eveit.web.db.app.entity.ProductionPageView;
import net.needlesslyabstract.eveit.web.service.ProductionPageService;
import net.needlesslyabstract.eveit.web.service.UserService;

@RestController
@RequestMapping("/productionPages")
public class ProductionPageController
		extends AbstractController<ProductionPage, ProductionPageView> {
	
	@Autowired
	ProductionPageService pages;
	
	@Autowired
	UserService users;

	// Needs to be transactional because we're lazy-loading the pages
	// for the user
	@GetMapping
	@Transactional(readOnly = true)
	public ResponseEntity<Collection<ProductionPageView>> list() {
		return viewToResponseEntity(pages.listForCurrentUser().map(toView::convert));
	}
	
	@PostMapping
	public ProductionPageView create(@RequestBody ProductionPage requestItem) {
		requestItem = pages.create(requestItem);
		return toView.convert(requestItem);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductionPageView> get(@PathVariable("id") Long id) {
		return dataToResponseEntity(pages.get(id));
	}
	
	@PatchMapping("/{id}")
	public ResponseEntity<ProductionPageView> update(@PathVariable("id") Long id, @RequestBody ProductionPage newValues) {
		return dataToResponseEntity(pages.update(id, newValues));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		return pages.delete(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
	}

}
