package net.needlesslyabstract.eveit.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionRequest;
import net.needlesslyabstract.eveit.web.db.game.entity.Type;
import net.needlesslyabstract.eveit.web.db.game.repository.TypeRepository;
import net.needlesslyabstract.eveit.web.db.view.ProductionRequestView;

@Component
public class ViewProductionRequest
		implements ViewConverterService<ProductionRequest, ProductionRequestView> {
	
	@Autowired
	private TypeRepository types;

	@Override
	public ProductionRequestView convert(ProductionRequest item) {		
		Type requestedType = types.findOne(item.getRequestedType());
		return new ProductionRequestView(item.getId(), item.getName(), item.getRequestedAmount(), requestedType, item.getSettings());
	}

}
