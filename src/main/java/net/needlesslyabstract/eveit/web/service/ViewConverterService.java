package net.needlesslyabstract.eveit.web.service;

import java.util.Collection;
import java.util.stream.Collectors;

@FunctionalInterface
public interface ViewConverterService<T, V> {
	
	public V convert(T item);
	
	public default <S extends T> Collection<V> convert(Collection<S> items) {
		return items.stream().map(this::convert).collect(Collectors.toList());
	}

}
