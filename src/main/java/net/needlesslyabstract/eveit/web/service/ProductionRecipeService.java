package net.needlesslyabstract.eveit.web.service;

import net.needlesslyabstract.eveit.web.db.view.ProductionRecipeCollection;

public interface ProductionRecipeService {
	
	public ProductionRecipeCollection get(int productTypeId);

}
