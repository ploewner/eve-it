package net.needlesslyabstract.eveit.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.needlesslyabstract.eveit.web.db.game.entity.SolarSystem;
import net.needlesslyabstract.eveit.web.db.game.repository.SolarSystemRepository;

@Component
public class SolarSystemServiceImpl implements SolarSystemService {
	
	@Autowired
	private SolarSystemRepository systems;

	@Override
	public List<SolarSystem> find(String name) {
		return systems.findByNameContaining(name);
	}

	@Override
	public SolarSystem get(int id) {
		return systems.findOne(id);
	}
	
}
