package net.needlesslyabstract.eveit.web.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import net.needlesslyabstract.eveit.web.Ownable;

public abstract class AbstractService<D extends Ownable, R extends JpaRepository<D, Long>> {
	
	@Autowired
	UserService users;
	
	@Autowired
	R items;
	
	@Transactional(readOnly = true)
	public Optional<D> get(Long id) {
		D item = items.findOne(id);
		return item == null || !item.isOwnedBy(users.getCurrentUser()) ?
				Optional.empty() :
				Optional.of(item);
	}
	
	@Transactional
	public boolean delete(Long id) {
		Optional<D> item = get(id);
		item.ifPresent(items::delete);
		return item.isPresent();
	}

}
