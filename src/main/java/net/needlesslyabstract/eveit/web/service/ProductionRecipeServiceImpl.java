package net.needlesslyabstract.eveit.web.service;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;

import net.needlesslyabstract.eveit.web.db.game.entity.Activity;
import net.needlesslyabstract.eveit.web.db.game.entity.ActivityBlueprint;
import net.needlesslyabstract.eveit.web.db.game.entity.Type;
import net.needlesslyabstract.eveit.web.db.game.repository.ActivityBlueprintRepository;
import net.needlesslyabstract.eveit.web.db.game.repository.ActivityRepository;
import net.needlesslyabstract.eveit.web.db.game.repository.MaterialRepository;
import net.needlesslyabstract.eveit.web.db.game.repository.SkillRepository;
import net.needlesslyabstract.eveit.web.db.game.repository.TypeRepository;
import net.needlesslyabstract.eveit.web.db.view.ProductionMaterial;
import net.needlesslyabstract.eveit.web.db.view.ProductionRecipe;
import net.needlesslyabstract.eveit.web.db.view.ProductionRecipeCollection;
import net.needlesslyabstract.eveit.web.db.view.ProductionSkill;

@Component
public class ProductionRecipeServiceImpl implements ProductionRecipeService {

	@Autowired
	private TypeRepository types;
	
	@Autowired
	private MaterialRepository materials;
	
	@Autowired
	private SkillRepository skills;
	
	@Autowired
	private ActivityRepository activities;
	
	@Autowired
	private ActivityBlueprintRepository blueprints;
	
	@Override
	@Transactional("gamedataTransactionManager")
	@SuppressWarnings("boxing")
	public ProductionRecipeCollection get(int productTypeId) {
		// TODO We create the view for a production request by
		// recursively descending into all needed materials.
		// Every recursion step does multiple lookups on the database,
		// which is horribly inefficient. It would be a lot better
		// if we didn't do that. However, getting multi-table joins
		// with composite keys on each table to work correctly in JPA
		// is a major PITA. At the point where the many lookups we do
		// in this method become a problem, it is very likely better
		// to just convert the game database to be better suited for
		// the kinds of lookups we want to do.
		// This would also get rid of some other problems, like not
		// being able to use foreign keys across databases without
		// looking things up manually, as well as the various
		// configuration issues when using JPA with multiple DBs.
		// TODO also, for some items (tested with EMP orbital strike),
		// this can produce an endless recursion
		
		Type productType = types.getOne(productTypeId);
		if(productType == null) return null;
		
		Map<Integer, ProductionRecipe> recipes = getRecipesFor(productType);
		
		Map<Integer, Type> allBlueprints = new HashMap<>();
		Map<Integer, Type> allSkills = new HashMap<>();
		
		recipes.values().stream().forEach(x -> recipeDfs(x, y -> {
			// Only add those blueprints that are used for Tech 1 production.
			// TODO hard-coding the activity IDs is ugly
			if(y.getActivityID() == 1 &&
					!y.getMainComponent().getRecipes().values().stream().filter(r -> r.getActivityID() == 8).findAny().isPresent())
				allBlueprints.put(y.getMainComponent().getType().getId(), y.getMainComponent().getType());
			y.getSkills().forEach((i, s) -> allSkills.put(i, s.getType()));
		}));
		
		return new ProductionRecipeCollection(recipes, allBlueprints, allSkills);
	}
	
	@SuppressWarnings("boxing")
	private Map<Integer, ProductionRecipe> getRecipesFor(Type product) {
		// TODO we're adding a buy node for blueprints here, which is not really
		// correct. we'd like a buy node for T3 main components though. We'll have
		// to see in the future if we just keep this and can make the distinction
		// in the client elegantly, or if we make the distinction here in case it's
		// not possible to do it elegantly in the client.
		// TODO hard-coding the activityID in the buy production recipe is ugly
		return Stream.concat(Stream.of(new ProductionRecipe(0, null, new HashMap<>(), new HashMap<>(), 0, null)),
					activities.findByProductId(product.getId()).stream().map(this::toRecipe))
				.collect(Collectors.toMap(ProductionRecipe::getActivityID, Function.identity()));
	}
	
	@SuppressWarnings("boxing")
	private ProductionRecipe toRecipe(Activity a) {
		Type mainComponentType = types.getOne(a.getTypeID());
		ProductionMaterial mainComponent = toMaterial(mainComponentType, 1);
		Map<Integer, ProductionMaterial> mats = materials.findByActivityIDAndTypeID(a.getActivityID(), a.getTypeID()).stream()
				.map(x -> toMaterial(x.getMaterialType(), x.getQuantity()))
				.collect(Collectors.toMap(x -> x.getType().getId(), Function.identity()));
		Map<Integer, ProductionSkill> skls = skills.findByActivityIDAndTypeID(a.getActivityID(), a.getTypeID()).stream()
				.map(x -> new ProductionSkill(x.getSkillType(), x.getLevel()))
				.collect(Collectors.toMap(x -> x.getType().getId(), Function.identity()));
		ActivityBlueprint bp = blueprints.findOne(mainComponentType.getId());
		return new ProductionRecipe(a.getActivityID(), mainComponent, mats, skls, a.getProducedPerRun(), bp != null ? bp.getMaxProductionLimit() : null);
	}
	
	private ProductionMaterial toMaterial(Type type, int rawAmount) {
		return new ProductionMaterial(type, rawAmount, getRecipesFor(type));
	}
	
	private void recipeDfs(ProductionRecipe recipe, Consumer<ProductionRecipe> f) {
		f.accept(recipe);
		ProductionMaterial mainComponent = recipe.getMainComponent();
		Stream.concat(mainComponent != null ? Stream.of(recipe.getMainComponent()) : Stream.empty(), recipe.getMaterials().values().stream())
				.flatMap(x -> x.getRecipes().values().stream())
				.forEach(x -> recipeDfs(x, f));
	}
	
}
