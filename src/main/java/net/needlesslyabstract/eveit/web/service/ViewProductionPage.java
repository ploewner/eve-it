package net.needlesslyabstract.eveit.web.service;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionPage;
import net.needlesslyabstract.eveit.web.db.app.entity.ProductionPageView;
import net.needlesslyabstract.eveit.web.db.app.repository.ProductionPageRepository;

@Component
public class ViewProductionPage implements ViewConverterService<ProductionPage, ProductionPageView> {
	
	@Autowired
	private ProductionPageRepository pages;

	@Override
	public ProductionPageView convert(ProductionPage item) {
		return pages.findOneProjectedById(item.getId());
	}
	
	@Override
	public <S extends ProductionPage> Collection<ProductionPageView> convert(Collection<S> items) {
		return pages.findProjectedByIdIn(items.stream().map(ProductionPage::getId).collect(Collectors.toList()));
	}

}
