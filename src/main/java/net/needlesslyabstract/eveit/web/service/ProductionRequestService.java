package net.needlesslyabstract.eveit.web.service;

import java.util.Collection;
import java.util.Optional;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionRequest;

public interface ProductionRequestService {

	Optional<Collection<ProductionRequest>> listForPage(Long pageId);

	Optional<ProductionRequest> create(Long pageId, ProductionRequest request);

	Optional<ProductionRequest> get(Long id);

	Optional<ProductionRequest> update(Long id, ProductionRequest request);

	boolean delete(Long id);

}
