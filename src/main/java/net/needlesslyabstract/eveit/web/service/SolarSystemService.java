package net.needlesslyabstract.eveit.web.service;

import java.util.List;

import net.needlesslyabstract.eveit.web.db.game.entity.SolarSystem;

public interface SolarSystemService {

	public List<SolarSystem> find(String name);
	
	public SolarSystem get(int id);

}
