package net.needlesslyabstract.eveit.web.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionPage;
import net.needlesslyabstract.eveit.web.db.app.entity.ProductionRequest;
import net.needlesslyabstract.eveit.web.db.app.entity.ProductionSettings;
import net.needlesslyabstract.eveit.web.db.app.repository.ProductionRequestRepository;

@Component
public class ProductionRequestServiceImpl
		extends AbstractService<ProductionRequest, ProductionRequestRepository>
		implements ProductionRequestService {
	
	@Autowired
	private ProductionPageService pages;
	
	@Override
	@Transactional(readOnly = true)
	public Optional<Collection<ProductionRequest>> listForPage(Long pageId) {
		return pages.get(pageId)
			.map(ProductionPage::getRequests);
	}

	@Override
	@Transactional
	public Optional<ProductionRequest> create(Long pageId, ProductionRequest request) {
		// TODO do some better validation on this. currently, this will return a 404
		// if the request does not have an item type associated. It should return
		// some other error code.
		if(request.getRequestedType() == null) return Optional.empty();
		return pages.get(pageId)
			.map(page -> {
				request.setId(null);
				request.setPage(page);
				ProductionSettings settings = request.getSettings();
				if(settings == null) {
					settings = new ProductionSettings();
					request.setSettings(settings);
				}
				if(settings.getBpID2MeLevel() == null)
					settings.setBpID2MeLevel(new HashMap<>());
				if(settings.getBpID2TeLevel() == null)
					settings.setBpID2TeLevel(new HashMap<>());
				if(settings.getSkillID2Level() == null)
					settings.setSkillID2Level(new HashMap<>());
				return items.save(request);
			});
	}

	@Override
	@Transactional
	public Optional<ProductionRequest> update(Long id, ProductionRequest newValues) {
		return get(id)
				.map(request -> request.mergeFrom(newValues))
				.map(items::save);
	}

}
