package net.needlesslyabstract.eveit.web.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionPage;
import net.needlesslyabstract.eveit.web.db.app.entity.User;
import net.needlesslyabstract.eveit.web.db.app.repository.ProductionPageRepository;

@Component
public class ProductionPageServiceImpl
		extends AbstractService<ProductionPage, ProductionPageRepository>
		implements ProductionPageService {
	

	@Override
	@Transactional(readOnly = true)
	public Optional<Collection<ProductionPage>> listForCurrentUser() {
		User currentUser = users.getCurrentUser();
		return currentUser != null ?
				Optional.of(currentUser.getPages()) :
				Optional.empty();
	}
	
	@Override
	@Transactional
	public ProductionPage create(ProductionPage item) {
		// TODO ProductionPage is the owning side of the relationship
		// User <-> ProductionPage. Do we still need to manually add the page to
		// the owning user because of caching isssues?
		item.setId(null);
		item.setOwner(users.getCurrentUser());
		
		return items.save(item);
	}
	
	@Override
	@Transactional
	public Optional<ProductionPage> update(Long id, ProductionPage newValues) {
		return get(id)
				.map(page -> page.mergeFrom(newValues))
				.map(items::save);
	}

}
