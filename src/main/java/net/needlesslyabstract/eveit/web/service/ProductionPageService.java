package net.needlesslyabstract.eveit.web.service;

import java.util.Collection;
import java.util.Optional;

import net.needlesslyabstract.eveit.web.db.app.entity.ProductionPage;

public interface ProductionPageService {

	Optional<Collection<ProductionPage>> listForCurrentUser();
	
	ProductionPage create(ProductionPage item);
	
	Optional<ProductionPage> get(Long id);
	
	Optional<ProductionPage> update(Long id, ProductionPage newValues);
	
	boolean delete(Long id);

}
