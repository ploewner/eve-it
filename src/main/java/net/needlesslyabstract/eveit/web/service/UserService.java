package net.needlesslyabstract.eveit.web.service;

import net.needlesslyabstract.eveit.web.db.app.entity.User;

public interface UserService {
	
	public User getCurrentUser();

}
