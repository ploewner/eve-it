package net.needlesslyabstract.eveit.web.service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import net.needlesslyabstract.eveit.web.db.app.entity.User;
import net.needlesslyabstract.eveit.web.db.app.repository.UserRepository;

@Component
public class UserServiceKeycloak implements UserService {
	
	@Autowired
	private UserRepository users;
	
	@Override
	@Transactional
	public User getCurrentUser() {
		return users.findOne(getCurrentUserHash())
				.orElseGet(() -> users.save(User.of(getCurrentUserHash())));
	}
	
	private static String getCurrentUserHash() {
		HttpServletRequest request =
				((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) request.getUserPrincipal();
		return token.getName();
	}

}
