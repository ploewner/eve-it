package net.needlesslyabstract.eveit.tools;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import net.needlesslyabstract.eveit.tools.db.entity.MarketGroup;
import net.needlesslyabstract.eveit.tools.db.entity.Type;
import net.needlesslyabstract.eveit.tools.db.repository.MarketRepository;

@SpringBootApplication
public class StaticDataExporter {
	
	public static final String DS_GAME_PACKAGE = "net.needlesslyabstract.eveit.tools";
	
	private final Logger logger = LoggerFactory.getLogger(StaticDataExporter.class);
	
	@Value("${tools.marketmenu.out}")
	private String marketMenuFile;
	
	public static void main(String... args) {
		new SpringApplicationBuilder(StaticDataExporter.class)
			.web(false)
			.run(args);
	}
	
	@Component
	public class CommandLine implements CommandLineRunner {
		
		@Autowired
		MarketRepository market;
		
		@Override
		public void run(String... args) throws Exception {
			File outFile = new File(marketMenuFile);
			logger.info("Trying to write market menu data to file: " + outFile.getAbsolutePath());
			if(outFile.exists()) {
				logger.info("File exists. Deleting.");
				outFile.delete();
			}
			
			logger.info("Fetching all market groups from database");
			Set<MarketGroup> rootGroups = market.findByParent(null);
			
			logger.info("Removing all items that can not be produced and all empty groups");
			pruneGroups(rootGroups);
			
			logger.info("Creating synthetic root market group");
			MarketGroup root = new MarketGroup();
			root.setId(null);
			root.setName("[root]");
			root.setChildren(rootGroups);
			root.setTypes(new HashSet<>());
			
			logger.info("Converting output and writing to file " + outFile.getAbsolutePath());
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.writeValue(outFile, root);
			
			logger.info("Done");
		}
		
		void pruneGroups(Iterable<MarketGroup> groups) {
			for(MarketGroup g : groups) {
				// Remove types that can't be produced
				for(Iterator<Type> i = g.getTypes().iterator(); i.hasNext();)
					if(i.next().getProducedBy() == null) i.remove();
				
				// Do the same recursively for all children
				pruneGroups(g.getChildren());
			}
			
			// We have processed groups and all their children. If that leaves
			// a group empty, then remove it
			for(Iterator<MarketGroup> i = groups.iterator(); i.hasNext();) {
				MarketGroup g = i.next();
				if(g.getChildren().isEmpty() && g.getTypes().isEmpty())
					i.remove();
			}
		}
		
	}
	
	@Configuration
	@EnableJpaRepositories(
		basePackages = DS_GAME_PACKAGE,
		entityManagerFactoryRef = "gamedataEntityManagerFactory"
	)
	public class GamedataConfig {
		
		@Bean
		@ConfigurationProperties(prefix = "gamedata.datasource")
		public DataSource gamedataDataSource() {
			return DataSourceBuilder.create().build();
		}
		
		@Bean
		@Primary
		public LocalContainerEntityManagerFactoryBean gamedataEntityManagerFactory(EntityManagerFactoryBuilder builder) {
			// TODO For some reason, neither are the properties from application.yml loaded correctly, nor are
			// they accessible from an @Autowired Environment.
			// For local testing, we just hardcode the properties right here. However, we'll have to work out those
			// problems before deployment (at which point we'll have to use a proper database anyway)
			// loading from an environment would probably be possible with the @PropertySource annotation
			LocalContainerEntityManagerFactoryBean em = builder.dataSource(gamedataDataSource())
					.packages(DS_GAME_PACKAGE)
					.properties(new HashMap<String, String>() {
						private static final long serialVersionUID = 4853597207175707084L;
						{
							put("sow-sql", "true");
							put("generate-ddl", "false");
							put("database-platform", "net.needlesslyabstract.eveit.SQLiteDialect");
							put("hibernate.dialect", "net.needlesslyabstract.eveit.SQLiteDialect");
						}
					})
					.build();
			return em;
		}
		
	}

}
