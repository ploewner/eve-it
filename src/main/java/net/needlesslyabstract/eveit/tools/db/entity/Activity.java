package net.needlesslyabstract.eveit.tools.db.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;

@Entity
@IdClass(ActivityID.class)
@Table(name = "industryActivity")
@SecondaryTables({
  @SecondaryTable(name="industryActivityProducts",
    pkJoinColumns = {
        @PrimaryKeyJoinColumn(name="typeID", referencedColumnName="typeID"),
        @PrimaryKeyJoinColumn(name="activityID", referencedColumnName="activityID")
    }
  )}
)
public class Activity {
	
	@Id
	private Integer activityID;
	
	@Id
	private Integer typeID;
	
	@OneToOne
	@JoinColumn(table = "industryActivityProducts", name = "productTypeID")
	private Type product;

	public Integer getActivityID() {
		return activityID;
	}

	public void setActivityID(Integer activityID) {
		this.activityID = activityID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Type getProduct() {
		return product;
	}

	public void setProduct(Type product) {
		this.product = product;
	}
	
}
