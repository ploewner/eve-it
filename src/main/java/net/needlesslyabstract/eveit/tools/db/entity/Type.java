package net.needlesslyabstract.eveit.tools.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "invTypes")
public class Type {

	@Id
	@Column(name = "typeID")
	private Integer id;
	
	@Column(name = "typeName")
	private String name;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "marketGroupID")
	private MarketGroup marketGroup;
	
	@JsonIgnore
	@OneToOne(mappedBy = "product")
	private Activity producedBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MarketGroup getMarketGroup() {
		return marketGroup;
	}

	public void setMarketGroup(MarketGroup marketGroup) {
		this.marketGroup = marketGroup;
	}

	public Activity getProducedBy() {
		return producedBy;
	}

	public void setProducedBy(Activity producedBy) {
		this.producedBy = producedBy;
	}
	
}
