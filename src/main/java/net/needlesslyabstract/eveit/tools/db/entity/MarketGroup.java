package net.needlesslyabstract.eveit.tools.db.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "invMarketGroups")
public class MarketGroup {
	
	@Id
	@Column(name = "marketGroupID")
	private Integer id;
	
	@Column(name = "marketGroupName")
	private String name;
	
	@OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
	private Set<MarketGroup> children;
	
	@ManyToOne
	@JoinColumn(name = "parentGroupID")
	@JsonIgnore
	private MarketGroup parent;

	@OneToMany(mappedBy = "marketGroup", fetch = FetchType.EAGER)
	private Set<Type> types;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<MarketGroup> getChildren() {
		return children;
	}

	public void setChildren(Set<MarketGroup> children) {
		this.children = children;
	}

	public MarketGroup getParent() {
		return parent;
	}

	public void setParent(MarketGroup parent) {
		this.parent = parent;
	}

	public Set<Type> getTypes() {
		return types;
	}

	public void setTypes(Set<Type> types) {
		this.types = types;
	}
	
	public void toJSON(StringBuffer sb, String parentIndent) {
		final String indent = parentIndent + "\t";
		sb.append("{\n");
		sb.append(indent);
		sb.append("name: '");
		sb.append(name);
//		sb.append("',\n");
//		sb.append(indent);
		sb.append(parentIndent);
		sb.append("}");
	}
	
}
