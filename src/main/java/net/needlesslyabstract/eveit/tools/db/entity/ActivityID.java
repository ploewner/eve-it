package net.needlesslyabstract.eveit.tools.db.entity;

import java.io.Serializable;

public class ActivityID implements Serializable {
	private static final long serialVersionUID = -3989925979543130102L;

	private Integer typeID;
	
	private Integer activityID;

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getActivityID() {
		return activityID;
	}

	public void setActivityID(Integer activityID) {
		this.activityID = activityID;
	}
	
}
