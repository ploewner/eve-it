package net.needlesslyabstract.eveit.tools.db.repository;

import java.util.Set;

import org.springframework.data.repository.Repository;

import net.needlesslyabstract.eveit.tools.db.entity.MarketGroup;

public interface MarketRepository extends Repository<MarketGroup, Integer> {
	
	Set<MarketGroup> findByParent(MarketGroup parent);

}