angular.module('navigation', [ 'ngRoute', 'auth', 'home', 'production' ])

.controller('NavigationCtrl', function($scope, $http, $location) {
	// Mark navigation LI elements as active / inactive, depending on whether
	// that page is currently shown in the main view
	$scope.getLiClass = function(path) {
		currentPath = $location.path()
		currentPath = currentPath == '' ? '/' : currentPath
		return path == currentPath ? 'active' : ''
	}
})
.config(function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl: 'js/home/home.html',
		controller: 'HomeCtrl'
	})
	.when('/production', {
		templateUrl: 'js/production/production.html',
		controller: 'ProductionCtrl'
	})
	.when('/settings', {
		templateUrl: 'js/settings/settings.html',
		controller: 'SettingsCtrl'
	})
	.otherwise('/')
})

.directive('navigationView', function() {
	return {
		restrict: 'A',
		controller: 'NavigationCtrl',
		templateUrl: 'js/navigation/navigation.html',
		scope: {}
	}
})