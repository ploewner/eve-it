/*
 * A very simple notification system to give users an indication when stuff is
 * being transferred to / from the server. Include a div with notification-view
 * attribute somewhere in the page where notifications should be shown.
 * 
 * Notifications can then be created by letting angular inject notificationData
 * and calling its 'forPromise' function, passing the notification message to
 * show and a promise.
 * The function will return a chained promise. For the original promise, it will
 * display an 'in progress' message while it was neither resolved nor rejected.
 * It will change that message to a success or error message when the promise
 * was resolved or rejected, respectively. The chained promise will be rejected
 * exactly if the original promise was rejected (since this module has no
 * capabilities to correct errors).
 */

angular.module('notification', [ 'settings', 'ui.bootstrap' ])

.factory('notificationData', function($q, settingsData) {
	var data = {
		settings: {
			showPending: true,
			showSucceeded: false,
			showFailed: true,
			popupOnlyErrors: false
		},
		
		pending: [],
		succeeded: [],
		failed: [],
		forResult: label => result => data.forPromise(label, $q.when(result)),
		forPromise: function(label, promise) {
			var handler = {
				label: label,
				state: 'pending',
				remove: function() {
					data[handler.state] = data[handler.state].filter(x => x != handler)
				}
			}
			data.pending.push(handler);
			return promise
				.then(xs => {
					handler.remove();
					handler.state = 'succeeded';
					if(data.settings.showSucceeded)
						data.succeeded.push(handler);
					return xs
				})
				.catch(xs => {
					handler.remove();
					handler.state = 'failed';
					handler.message = xs;
					if(data.settings.showErrors);
						data.failed.push(handler);
					// Propagate the error down the promise chain. We didn't
					// correct anything!
					return $q.reject(xs)
				})
		},
	}
	settingsData.addSection('Notification Settings', data.settings, 'js/notification/forms/settings.html');
	return data;
})

.controller('NotificationCtrl', function($scope, notificationData) {
	$scope.data = notificationData
	$scope.settings = notificationData.settings
	$scope.close = handler => handler.remove()
})

.directive('notificationView', function() {
	return {
		restrict: 'A',
		controller: 'NotificationCtrl',
		templateUrl: 'js/notification/notification.html'
	}
})