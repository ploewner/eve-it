angular.module('util', ['production', 'ui.select', 'ngSanitize', 'esi'])

/*
 * Passing down static data about available decryptors through multiple isolated
 * scopes would be very ugly, so we define a directive that allows to select a
 * decryptor for an invention process.
 */
.controller("DecryptorSelectCtrl", function($scope, Decryptors) {
	// TODO there's two things ugly about this:
	// - initializing based on a future might not be the best idea
	// - more severe: lack of proper binding to the ID; if something
	// else changes the decryptor, the select won't notice
	Decryptors.all.then(decryptors => {
		$scope.decryptors = decryptors;
		$scope.decryptorArray = [];
		for(i in decryptors)
			$scope.decryptorArray.push(decryptors[i]);
		
		$scope.select = { item: $scope.decryptors[$scope.model] };
		
		$scope.selected = function() {
			$scope.model = $scope.select.item.id;
		}
	});
})
.directive("decryptorSelect", function() {
	return {
		restrict: 'E',
		controller: 'DecryptorSelectCtrl',
		templateUrl: 'js/util/decryptor-select.html',
		scope: {
			model: '=',
			disabled: '='
		}
	}
})

/*
 * Form control to select a skill level between 0 and 5 via graphical input
 * 
 * TODO still shows pointer cursor on disabled elements, but shouldn't
 * TODO if the value of override changes while the mouse is hovering
 * 		over the element, then the shown behavior (preview of new value)
 * 		behaves as if override still had the old value. This isn't really
 * 		bad since the value doesn't change at the moment, but we might
 * 		want to look into this as a low-priority issue.
 */
.controller("SliderCtrl", function($scope) {
	var elements = $scope.max / $scope.step;
	var activeElement = $scope.model / $scope.step;
	$scope.previewValue = $scope.model;
	
	$scope.bars = [];
	for(var i = 1; i <= elements; i++) {
		$scope.bars.push({ value: i });
	};
	
	$scope.setStyle = function(index, activeClass) {
		for(var i = 1; i <= elements; i++)
			$scope.bars[i-1].cls = (i <= index) ? activeClass : 'bg-default';
		$scope.previewValue = index * $scope.step;
	}
	$scope.resetStyle = function() {
		$scope.setStyle(activeElement, $scope.disabled ? 'bg-disabled' : 'bg-primary');
	};
	$scope.setPreviewStyle = function(index) {
		if($scope.disabled) return;
		if(index == activeElement) index = 0;
		$scope.setStyle(index, 'bg-info');
	};
	$scope.setValue = function(index) {
		if($scope.disabled) return;
		if(index == activeElement) index = 0;
		activeElement = index;
		$scope.resetStyle();
		$scope.model = $scope.previewValue;
	};
	
	$scope.resetStyle();
})
.directive("slider", function() {
	return {
		restrict: 'E',
		controller: 'SliderCtrl',
		templateUrl: 'js/util/slider.html',
		scope: {
			model: '=',
			step: '=',
			max: '=',
			disabled: '='
		}
	}
})

/*
 * Provides a form where users can choose to activate or deactivate a slider for
 * each item in a set of items.
 * 
 * This was added so that users can not only set a default ME level, TE level or
 * skill level, but override the default levels for blueprints or skills by a
 * value that's individual for every blueprint or skill.
 */
.controller("MultiSliderCtrl", function($scope, ProductionCalculator) {
	// Requests usually have a ProductionCalculator attached, but not
	// if we're currently showing the request creation modal.
	var calc = $scope.request.calculator
			? $scope.request.calculator
			: new ProductionCalculator($scope.request);
	$scope.itemsArray = [];
	for(i in $scope.items) $scope.itemsArray.push($scope.items[i]);
	
	$scope.selected = { items: [] }
	for(i in $scope.model) $scope.selected.items.push($scope.items[i]);
	
	$scope.onSelect = function(item) {
		$scope.model[item.id] = calc[$scope.defaultProperty];;
	}
	$scope.onRemove = function(item) {
		delete $scope.model[item.id];
	}
})
.directive("multiSlider", function() {
	return {
		restrict: 'E',
		controller: 'MultiSliderCtrl',
		templateUrl: 'js/util/multi-slider.html',
		scope: {
			items: '=',
			label: '=',
			max: '=',
			step: '=',
			model: '=',
			request: '=',
			defaultProperty: '='
		}
	}
})

/*
 * Provides a menu to search for and select solar systems.
 * 
 * The model will only contain the selected system's id.
 */
.controller("SystemSelectCtrl", function($scope, $http) {
	$scope.selected = { system: null }
	if($scope.model !== null && typeof $scope.model !== "undefined") {
		$http.get('/systems/' + $scope.model)
			.then(function(response) {
				$scope.selected.system = response.data;
			})
	}
	$scope.foundSystems = [];
	
	$scope.findSystems = function(name) {
		return $http.get('/systems/find/' + name)
			.then(function(response) {
				$scope.foundSystems = response.data;
			});
	}
	$scope.onSelect = function(item) {
		if(item == null) $scope.onRemove();
		else $scope.model = item.id;
	}
	$scope.onRemove = function() {
		$scope.model = null;
	}
})
.directive("systemSelect", function() {
	return {
		restrict: 'E',
		controller: 'SystemSelectCtrl',
		templateUrl: 'js/util/system-select.html',
		scope: {
			model: '=',
			disabled: '='
		}
	}
})

/*
 * Provides a form element that can be used to input integers via text.
 */
.controller("IntInputCtrl", function($scope) {
	$scope.inputText = $scope.model;
	$scope.onChange = function() {
		if($scope.form[$scope.id].$valid)
			$scope.model = parseInt($scope.inputText);
	}
})
.directive("intInput", function() {
	return {
		restrict: 'E',
		controller: 'IntInputCtrl',
		templateUrl: 'js/util/int-input.html',
		scope: {
			model: '=',
			disabled: '=',
			id: '=',
			form: '='
		}
	}
})

/*
 * Provides a form element that can be used to input floats via text.
 */
.controller("FloatInputCtrl", function($scope) {
	$scope.inputText = $scope.model;
	$scope.onChange = function() {
		if($scope.form[$scope.id].$valid)
			$scope.model = parseFloat($scope.inputText);
	}
})
.directive("floatInput", function() {
	return {
		restrict: 'E',
		controller: 'FloatInputCtrl',
		templateUrl: 'js/util/float-input.html',
		scope: {
			model: '=',
			disabled: '=',
			id: '=',
			form: '='
		}
	}
})

/*
 * Displays a monetary value in red or black depending on whether it's negative
 * or positive.
 */
.controller('IskValueCtrl', function($scope, $filter) {
	$scope.$watch('model', function(value) {
		$scope.cls = (typeof value === "undefined" || value === null || isNaN(value) || value < 0)
				? 'isk-negative'
				: 'isk-positive';
		// Need to filter here because if we did it in the template then the
		// filter would just swallow invalid values
		$scope.val = (typeof value === "undefined" || value === null || isNaN(value))
				? NaN
				: $filter('number')(value, 2);
	})
})
.directive('iskValue', function() {
	return {
		restrict: 'E',
		controller: 'IskValueCtrl',
		template: '<span ng-class="cls">{{ val }}</span> ISK',
		scope: {
			model: '='
		}
	}
})