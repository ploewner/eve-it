/*
 * This module is responsible for logging the user in and out. It provides the
 * authView directive, which can be used to display the current login status and
 * user controls.
 * In addition, it provides the 'Auth' object that can be injected via angular.
 * This object contains data that allows other modules to check authentication
 * status, register listeners to authentication status and request to log in or
 * log out users.
 * 
 * Internally, this module uses the API of keycloak.js, as provided by the
 * keycloak SSO server. It encapsulates all controls, token refreshes and other
 * necessary glue needed to use keycloak. This module also takes care to inject
 * valid SSO tokens into HTTP requests made to the origin server.
 */

/*
 * This is a bit hacky, but seems to work best:
 * We need to wait until keycloak is completely initialized before we initialize
 * the auth module. However, keycloak returns a promise for its initialization.
 * Angular won't let us simply return that promise, then inject the result when
 * calling the Auth factory. Thus we'd have to return a promise from that
 * factory, and in turn we'd have to return promises again from every other
 * factory where the Auth object is injected. This is overall very, very ugly.
 * We went with this still hacky, but much cleaner solution: Provide the
 * following function to initialize keycloak (thus keeping it confined in this
 * module's file), call this function from the main module and bootstrap angular
 * only once the promise resolves.
 */
var keycloak = {};
var initKeycloak = function() {
	keycloak = new Keycloak('data/keycloak-client.json');
	// check-sso: We don't require users to be logged in to access the page,
	// but if they're already logged in to the keycloak server we log them in
	// here as well.
	return keycloak.init({ onload: 'check-sso' });
}

angular.module('auth', [])

/*
 * Provides data about the current authentication state and functions to control
 * it.
 */
.factory('Auth', function($rootScope) {
	// Used to call listeners to the authentication state.
	// Keycloak callbacks are not executed in an angularJS context, thus we need
	// to make sure that model updates we make are digested by angularJS
	var updateState = values => {
		$rootScope.$apply(() => {
			for(i in values) data[i] = values[i];
		});
		var listenerFunction = chooseListenerFunction();
		for (i in data.listeners) data.listeners[i][listenerFunction]();
	};
	
	// Current login state and controls
	var data = {
		username: null,
		expired: false,
		errorMessage: null,
		authenticated: () => data.username != null,
		listeners: [],
		addListener: listener => {
			data.listeners.push(listener);
			// Make sure that the listener knows the current state
			listener[chooseListenerFunction()](data);
		},
		// Have to call on keycloak here, or else keycloak won't get
		// a valid this pointer
		login: () => keycloak.login(),
		logout: () => keycloak.logout(),
		refreshToken: () =>	keycloak.updateToken(5).success(() => updateState({ expired: false })),
		keycloak: keycloak
	};

	// Which function on a listener do we need to call depending on state?
	var chooseListenerFunction = function() { return data.authenticated() ? 'authLoggedIn' : 'authLoggedOut' };
	
	// Register to keycloak client in order to change state according to events
	keycloak.onAuthSuccess = function() {
		keycloak.loadUserProfile().success(profile => {
			updateState({ errorMessage: null, username: profile.username })
		}).error(() => {
			keycloak.clearToken();
			updateState({ errorMessage: "Unable to fetch user profile. Logging you back out." });
		})
	}
	keycloak.onAuthRefreshError = function() {
		keycloak.clearToken();
		updateState({ errorMessage: "You have been inactive for too long and have been logged out." });
	}
	keycloak.onTokenExpired = function() {
		// In the view, we provide a way to call status.refreshToken so that
		// the user can refresh the auth token without reloading, for expample
		// when editing a long form
		updateState({ expired: true })
	}
	keycloak.onAuthLogout = function() {
		updateState({ username: null, errorMessage: null, expired: false })
	}
	
	// We could already be logged in, but never noticed it because the listener
	// wasn't registered at that time
	if(keycloak.authenticated) if(keycloak.authenticated) keycloak.onAuthSuccess();
	
	return data;
})

/*
 * Intercepts HTTP requests to the origin server to inject a valid
 * authentication token. Refreshes the token beforehand if necessary.
 * If we're not currently logged in, then requests to any server are not
 * modified.
 * 
 * Warning: Do NOT make this send tokens to any other servers. There is no
 * security mechanism in place preventing a third party (and potentially
 * malicious) server or man-in-the-middle to receive a valid token from us and
 * use it for themselves.
 */
.factory('authTokenInterceptor', function($q, $location, Auth) {
	var keycloak = Auth.keycloak;
	return {
		request: function(config) {
			var loc = window.location, a = document.createElement('a')
			a.href = config.url
			var sameOrigin =
				a.hostname === loc.hostname &&
				a.port === loc.port &&
				a.protocol === loc.protocol;
				
			if(keycloak.token && sameOrigin) {
				var deferred = $q.defer();
				keycloak.updateToken(5).success(function() {
					config.headers = config.headers || {};
					config.headers.Authorization = 'Bearer ' + keycloak.token;
					deferred.resolve(config);
				}).error(function() {
					deferred.reject('Failed to refresh token');
				});
				return deferred.promise;
			} else {
				return config
			}
		}
	}
})
.config(function($httpProvider) {
	// Use the interceptor defined above to infuse calls with our auth token
	$httpProvider.interceptors.push('authTokenInterceptor')
})

/*
 * This directive can be used to show the user current authentication state as
 * well as display controls to log the user in or out
 */
.controller('AuthViewCtrl', function($scope, Auth) {
	$scope.data = Auth;
})
.directive('authView', function() {
	return {
		restrict: 'A',
		controller: 'AuthViewCtrl',
		scope: {},
		templateUrl: 'js/auth/auth.html'
	}
})