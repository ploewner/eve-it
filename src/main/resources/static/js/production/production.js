angular.module('production', [ 'auth', 'ngResource', 'ui.bootstrap', 'repository', 'notification' ])

/*
 * We need to know how to produce any of those item that the user requests. The
 * recipes to produce items are decoupled from the actual request storage and
 * must be queried from the server. Since such a query puts load on the server
 * and the network (especially if loading a page with a lot of requests), we
 * cache recipes that we already know.
 * Note: Recipes are decoupled from requests because in a previous design, we
 * ran into problems when sending back the recipe for an item request hard-coded
 * into that request. In particular, letting the server fill out the recipe
 * requires us to wait until we created a request to know how it is going
 * to be produced, so it is not possible to manipulate settings that depend on
 * knowing the recipe during request creation. 
 */
.factory('ProductionRecipes', function($http, notificationData, $q) {
	// We're storing the HTTP request promises in cache.data, since we'd
	// have to return a promise either way even if the recipe is already
	// cached (consistency of the API).
	var cache = {
		data: {}
	}
	cache.find = function(type) {
		if(typeof cache.data[type.id] === "undefined")
			cache.data[type.id] =
				notificationData.forPromise('Fetching recipe for ' + type.name, $http({
					method: 'GET',
					url: '/productionRecipes/' + type.id
				}).then(response => response.data));
		return cache.data[type.id];
	}
	cache.clear = function() {
		cache.data = {};
	}
	return cache;
})
/*
 * We operate on two main units of data: Pages and requests. Pages and requests
 * form a one-to-many relationship.
 * Pages serve to structure the individual requests both for clarity for the
 * user and to avoid holding too many requests in memory at the same time and
 * having to update them all.
 * Requests represent the want to produce a certain item with certain settings
 * used for its production. They store static data, settings and the results of
 * computing ISK and and time costs for the item's production.
 */
.factory('ProductionPage', function($resource) {
	return $resource('/productionPages/:id', {id: '@id'}, {
		update: { method: 'PATCH', url: '/productionPages/:id' }
	})
})
.factory('ProductionPageRepository', function(ProductionPage, Repository) {
	return function() {
		return new Repository(ProductionPage,
			{
				labelUc: 'Page',
				labelLc: 'page',
				labelPlLc: 'pages',
				templateUrl: 'js/production/forms/ProductionPage.html',
				transientProperties: [ 'requests' ],
				defaultProperties: {
					name: '[New Page]',
					settings: {
						defaultMeLevel: 10,
						defaultTeLevel: 20,
						defaultSkillLevel: 5,
						decryptorID: 0
					}
				}
			}
		);
	}
})
.factory('ProductionRequest', function($resource) {
	return function(page) {
		return $resource('/productionRequests/:id', {id: '@id'}, {
			query: { method: 'GET', url: '/productionRequests/list/' + page.id, isArray: true },
			save: { method: 'POST', url: '/productionRequests/' + page.id },
			update: { method: 'PATCH', url: '/productionRequests/:id' }
		});
	}
})
.factory('ProductionRequestRepository', function(ProductionRequest, Repository, ProductionRecipes, $q) {
	/*
	 * Every request instance needs to know how it can be produced. Since the
	 * recipes are detached from the requests, we have to ask the server how to
	 * do that.
	 * Note: If anything is added to the request in onEachInstance, note that
	 * it will only be present on _already created_ instances. If the same
	 * properties are needed in the 'create item' form, then they should be
	 * added to new items (i.e. pre server creation request) as well. This is
	 * done further below in the repository definition. 
	 */
	var recipeLookupListener = {
		onEachInstance: function(request) {
			request.acquisitionPromise = ProductionRecipes.find(request.type)
			request.acquisitionPromise.then(acquisition => {
				request.acquisition = acquisition;
			})
		}
	}
	var staticListeners = [ recipeLookupListener ];
	// Expand Repository; We can't simply use defaultProperties to set
	// properties for newly created pages, since the default name and type id
	// depend on the type being produced. Since the repository definition is
	// essentially the client-side type definition for entities, overriding
	// the create method seems to be the right thing to do instead of directly
	// calling it from the view.
	// TODO it would be cleaner to do this by actually extending Repository
	// instead of replacing the create method on individual instances, but
	// we don't have many instances active at any one point in time so it's
	// not really a priority
	var RequestRepository = function(page) {
		var repository = new Repository(new ProductionRequest(page),
			{
				labelUc: 'Request',
				labelLc: 'request',
				labelPlLc: 'requests',
				templateUrl: 'js/production/forms/ProductionRequest.html',
				// Note: calculator is a ProductionCalculator that adds itself via listener
				transientProperties: [ 'type', 'acquisition', 'acquisitionPromise', 'calculator' ]
			}
		);
		repository.createForType = function(type) {
			ProductionRecipes.find(type).then(acquisition => {
				this.create({name: type.name, requestedType: type.id, requestedAmount: 1, acquisitionPromise: $q.when(acquisition), acquisition: acquisition, settings: {
					bpID2MeLevel: {}, bpID2TeLevel: {}, skillID2Level: {}
				}})
			});
		}
		for(i in staticListeners) repository.addListener(staticListeners[i]);
		return repository;
	}
	
	RequestRepository.addListener = function(listener) {
		staticListeners.push(listener);
	}
	
	return RequestRepository;
})

/*
 * Serves to persist data even if the user changes the view and then returns to
 * the production view.
 */
.factory('productionData', function($http, $q, ProductionPageRepository, ProductionRequestRepository, Auth, notificationData, ProductionRecipes) {
	var data = {
		authStatus: 'invalid',
		pages: null,
		_currentPage: null,
		marketGroups: {
			root: null,
			current: null
		}
	};
	Object.defineProperty(data, 'currentPage', {
		get: function() { return data._currentPage },
		set: function(page) {
			// Forget about previous pages' requests to save some memory
			if(data._currentPage) delete data._currentPage.requests
			
			data._currentPage = page
			if(page == null) return;
			
			page.requests = new ProductionRequestRepository(page)
		}
	});
	
	// TODO maybe we could migrate the auth listener system to using a promise
	// right in that module?
	var loginDeferred = $q.defer();
	data.loginStatus = loginDeferred.promise;
	data.authLoggedIn = function() {
		this.authStatus = 'finished';
		this.pages = new ProductionPageRepository();
		loginDeferred.resolve();
 	};
	data.authLoggedOut = function() {
		/*
		 * Clean up after a logout has occurred so we don't bleed data to an
		 * unauthorized user. This may not seem necessary at first glance, as
		 * keycloak's default logout is based on a page reload, however users
		 * can be logged out after long periods of inactivity, at which point
		 * there is no page reload.
		 */
		this.authStatus = 'cleared';
		this.pages = null;
		this.currentPage = null;
		ProductionRecipes.clear();
		loginDeferred.resolve();
	};
	
	Auth.addListener(data);
	
	// Load market menu data and process it to display the market menu
	notificationData.forPromise('Loading market data', $http({
		method: 'GET',
		url: '@tools.marketmenu.url@'
	}).then(response => {
		var setParent = parent => node => {
			node.parent = parent;
			node.children.map(setParent(node));
		}
		setParent(null)(response.data);
		data.marketGroups.root = response.data;
		data.marketGroups.current = response.data;
	}));
	
	return data;
})

/*
 * TODO this comment is no longer up to date. as long as the update method is
 * not called on the calculator, it's pretty light weight so we can use it
 * on-the-fly to load properties
 * 
 * Generally, due to our repository system, our requests and pages suffer from
 * anemic domain model: We can't simply give them arbitrary prototypes if those
 * prototypes should calculate something based on the current page (i.e. which
 * is the default value for a skill level), because then the repositories would
 * have to depend on page data. This would lead to a cyclic dependency that can
 * not be resolved by angular's injection system.
 * In addition, we only have the possibility of declaring properties as
 * transitive on the top level item (page or request), and can't do that for any
 * of their properties (we could still probably mark defined properties as
 * non-enumerable to get them ignored by the repository?).
 * Either way, parsing the object trees returned by the server and trying to
 * inject the right methods / prototypes at the right places would be ugly.
 * 
 * ProductionCalculator circumvents this issue. It provides a compact API
 * for defining and querying all kinds of values that can't just be properties
 * of pages or requests for the reasons stated above.
 * 
 * When extending this, note that the object should be as light-weight as
 * possible; It should not save internal state except things that cannot change
 * and are expensive to compute, for caching purposes.
 * The goal here is to have a prototype function that can be fed a request and
 * will calculate everything we need to know about that request.
 */
.factory('ProductionCalculator', function($http, $q, $filter, productionData, notificationData, Decryptors, Activities, AdjustedCosts, CostIndices, MarketData) {
	/*
	 * Accumulates similar expenses (or profits) during a calculation. Once
	 * everything has been accumulated, the finalize function can be called,
	 * which will propagate expenses and profits in the parent groups bottom-up.
	 * 
	 * Groups can contain sub-groups, whose total will be added to their parent.
	 * Groups can contain items, which are the objects that actually accumulate
	 * values during computation. An item is mapped by a key and it's created
	 * the first time that the key is used. In the GUI, groups and items are
	 * labeled by either the info property, or info.name, if it exists.
	 * 
	 * Items accumulate two kinds of values: 'value' is the cost or profit per
	 * produced item. 'count' is the number of materials of the item's type used
	 * to produce the item requested by the user. 'count' is not used for
	 * anything but materials, and will be invalidated because non-materials
	 * pass undefined as its value.
	 */
	function ExpenseGroup(info, producedAmt, expandable) {
		this.info = info;
		this.producedAmt = producedAmt;
		this.expandable = expandable;
		this.expanded = expandable;
		this.subGroups = [];
		this.items = {};
	}
	ExpenseGroup.prototype = {}
	ExpenseGroup.prototype.toggle = function() { if(this.expandable) this.expanded = !this.expanded }
	ExpenseGroup.prototype.addSubGroup = function(expenseGroup) {
		this.subGroups.push(expenseGroup);
		return expenseGroup;
	}
	ExpenseGroup.prototype.addItem = function(key, info, value, count) {
		// Using if/else instead of initializing to 0 and always adding because
		// count needs to be invalid (undefined) for every item that doesn't use
		// it.
		if(!this.items.hasOwnProperty(key))
			this.items[key] = { info: info, value: value, count: count }
		else {
			this.items[key].value += value;
			this.items[key].count += count;
		}
	}
	ExpenseGroup.prototype.addItemAll = function(key, info, value, count) {
		this.addItem(key, info, value / this.producedAmt, count / this.producedAmt);
	}
	ExpenseGroup.prototype.finalize = function() {
		this.value = 0;
		for(i in this.subGroups) {
			var x = this.subGroups[i];
			x.finalize();
			this.value += x.value;
		}
		for(i in this.items) {
			var x = this.items[i];
			x.valueAll = x.value * this.producedAmt;
			x.countAll = x.count * this.producedAmt;
			this.value += x.value;
		}
		this.valueAll = this.value * this.producedAmt;
	}
	ExpenseGroup.prototype.countItems = function() { return Object.keys(this.items).length }
	
	/*
	 * Accumulates errors and warnings that can happen during a result
	 * calculation. An error group can have one prefix and one postfix message,
	 * as well as multiple infix messages. A single error can be displayed in
	 * its own panel by only setting the prefix message, multiple similar errors
	 * can be displayed by setting prefix and / or postfix messages, then adding
	 * the individual messages as infix when they occur. The infix messages will
	 * be displayed as a list in this ErrorGroup's panel, between prefix and
	 * postfix message.
	 * 
	 * WARNING: Currently all our ErrorGroups that can display multiple errors
	 * have pre- and postfix and shouldn't be displayed if they have no infix.
	 * All single errors have no postfix, so postfix existence and infix length
	 * decide whether the group is displayed in the GUI.
	 */
	function ErrorGroup(isWarning, prefix, postfix) {
		this.isWarning = isWarning;
		this.prefix = prefix;
		this.postfix = postfix;
		this.infix = [];
	}
	ErrorGroup.prototype.addInfix = function(infix) {
		// In case an infix occurs multiple times, we need to take care to
		// display it only once, or else angular's ng-repeat will choke.
		if(!this.infix.includes(infix)) this.infix.push(infix);
	}
	
	/*
	 * Accumulates and stores the result of a calculation: Which errors and
	 * warnings did occur during the calculation? Which expense groups are
	 * required to produce the requested items?
	 * 
	 * Encapsulating this in a single object has the advantage of not having to
	 * care about what data we throw away for a new calculation, possibly
	 * forgetting some of it and displaying parts of the result of an old
	 * calculation.
	 */
	function CalculationResult(amt) {
		// Don't need an array for the expense groups as they're in tree form and
		// profit is the root node
		this.profit = new ExpenseGroup('Profit', amt, true);
		this.sales = this.profit.addSubGroup(new ExpenseGroup('Sales revenue', amt, false));
		this.fees = this.profit.addSubGroup(new ExpenseGroup('Fees', amt, true));
		this.materials = this.profit.addSubGroup(new ExpenseGroup('Materials', amt, true));
		
		// These contain ErrorGroups. Storing them in different arrays so that
		// we can display errors before warnings in the GUI without much hassle
		this.warnings = [];
		this.errors = [];
		
		// Prepare error accumulators. Keeping a named reference so we can
		// access them easier in the calculation. Adding them to the arrays as
		// well so we can display them easier in the GUI
		this.missingSettingWarnings = this.addErrorGroup(new ErrorGroup(true,
				'The following settings are missing:',
				'We still calculated a result, but it will be off. Adjust your settings to obtain more accurate results.'));
		this.missingSettingErrors = this.addErrorGroup(new ErrorGroup(false,
				'The following settings are missing:',
				'These are vital settings, we can not calculate a result.'));
		this.unavailableItemErrors = this.addErrorGroup(new ErrorGroup(false,
				'The following items are not available on the market:',
				'We can not produce the requested item.'));
	}
	CalculationResult.prototype = {}
	CalculationResult.prototype.addErrorGroup = function(group) {
		(group.isWarning ? this.warnings : this.errors).push(group);
		return group;
	}
	
	function Calculator(request) {
		this.request = request;
		this.productionData = productionData;
	}
	Calculator.prototype = {};
	
	var defaultSettingAccessor = function(propName) {
		Object.defineProperty(Calculator.prototype, propName, {
			get: function() {
				return (this.request.settings[propName] == null || typeof this.request.settings[propName] === "undefined")
					? this.productionData.currentPage.settings[propName]
					: this.request.settings[propName];
			}
		})
	}
	defaultSettingAccessor('defaultMeLevel');
	defaultSettingAccessor('defaultTeLevel');
	defaultSettingAccessor('defaultSkillLevel');
	defaultSettingAccessor('sellSystemID');
	defaultSettingAccessor('buySystemID');
	defaultSettingAccessor('brokerFee');
	defaultSettingAccessor('productionSystemID');
	defaultSettingAccessor('salesTax');
	defaultSettingAccessor('manufacturingCostIndex');
	defaultSettingAccessor('copyingCostIndex');
	defaultSettingAccessor('inventionCostIndex');
	defaultSettingAccessor('facilityTax');
	
	var overrideSettingAccessor = function(propName, defaultPropName) {
		Calculator.prototype[propName] = function(typeId) {
			if(typeof this.request.settings[propName][typeId] !== 'undefined')
				return this.request.settings[propName][typeId];
			return this[defaultPropName];
		}
	}
	overrideSettingAccessor('bpID2MeLevel', 'defaultMeLevel');
	overrideSettingAccessor('bpID2TeLevel', 'defaultTeLevel');
	overrideSettingAccessor('skillID2Level', 'defaultSkillLevel');
	
	// Several helper functions for the update function
	Calculator.prototype.fetchSystem = function(result, propName, errorLabel, fetchSettings, ignoreNotPresent) {
		if(this[propName + 'ID'])
			fetchSettings[propName] = $http.get('/systems/' + this[propName + 'ID'])
					.then(response => response.data);
		else if(!ignoreNotPresent) result.missingSettingErrors.addInfix(errorLabel);
	}
	// Helper functions for recipe traversal
	Calculator.prototype.traverseMaterial = function(material, recipe, callbacks, activeOnly) {
		if(callbacks.beforeMaterial) callbacks.beforeMaterial(material, recipe);
		if(activeOnly)
			this.traverseRecipe(material.activeRecipe, material, callbacks, activeOnly);
		else
			for(i in material.recipes) this.traverseRecipe(material.recipes[i], material, callbacks, activeOnly);
		if(callbacks.afterMaterial) callbacks.afterMaterial(material, recipe);
	}
	Calculator.prototype.traverseRecipe = function(recipe, material, callbacks, activeOnly) {
		if(callbacks.beforeRecipe) callbacks.beforeRecipe(recipe, material);
		for(i in recipe.materials) this.traverseMaterial(recipe.materials[i], recipe, callbacks, activeOnly);
		if(callbacks.afterRecipe) callbacks.afterRecipe(recipe, material);
	}
	Calculator.prototype.traverseRecipes = function(acquisition, callbacks, activeOnly) {
		this.traverseMaterial(acquisition, null, callbacks, activeOnly);
	}
	Calculator.prototype.round = function(n, digits) {
		// Straight from stackoverflow
		if (digits === undefined) digits = 0;

		var multiplicator = Math.pow(10, digits);
		n = parseFloat((n * multiplicator).toFixed(11));
		var test =(Math.round(n) / multiplicator);
		return +(test.toFixed(digits));
	}
	Calculator.prototype.neededMaterials = function(runs, baseAmount, meMod) {
		return Math.max(runs, Math.ceil(this.round(runs * baseAmount * meMod, 2)));
	}
	Calculator.prototype.update = function() {
		var amt = this.request.requestedAmount;
		var result = new CalculationResult(amt);
		// Can't set this.result later, as we abort the calculation on critical
		// errors that wouldn't be shown otherwise
		this.result = result;
		
		// Currently not setting this in CalculationResult's constructor since
		// strictly speaking, the messages are a property of the calculation
		// algorithm
		result.addErrorGroup(new ErrorGroup(true, 'Currently not accounting for T2 invention'));
		result.addErrorGroup(new ErrorGroup(true, 'Currently not accounting for T3 reverse engineering'));
		
		// We store intermediate values directly in the recipe. Instead of
		// trying to clear it on every new calculation (which we WILL
		// inevitably forget for some value or another, producing obscure
		// untraceable errors), simply create a new one that we use in this
		// calculation.
		var fetchSettings = {
			acquisition: this.request.acquisitionPromise.then(x => angular.copy(x))
		};
		
		this.fetchSystem(result, 'sellSystem', 'Sell system', fetchSettings);
		this.fetchSystem(result, 'buySystem', 'Buy system', fetchSettings);
		this.fetchSystem(result, 'productionSystem', 'Production system', fetchSettings, true);
		
		// If we don't know the prices, we can't calculate anything at all.
		if(result.missingSettingErrors.infix.length != 0) return;
		
		var fetchData = {
			settings: notificationData.forPromise('Preparing settings for ' + this.request.name, $q.all(fetchSettings))
		}
		fetchData.settingsProcessed = fetchData.settings
			.then(values => {
				// Show the first node of the production tree in the GUI
				result.acquisition = values.acquisition;
				values.acquisition.expanded = true;
				// Make acquisition an actual material node so we can handle
				// it like any other
				result.acquisition.type = this.request.type;
				result.acquisition.requestedAmount = this.request.requestedAmount;
				// We're currently assuming instant buying and selling
				// via standing order
				fetchData.sellPrices = MarketData.annotateSellTypes([ this.request.type ], values.sellSystem);
				
				var allTypes = [];
				this.traverseRecipes(values.acquisition, { beforeMaterial: x => allTypes.push(x.type) });
				fetchData.buyPrices = MarketData.annotateBuyTypes(allTypes, values.buySystem)
				
				if(values.productionSystem)
					fetchData.costIndices = CostIndices.annotateSystems([ values.productionSystem ]);
				
				fetchData.adjustedCosts = AdjustedCosts.annotateTypes(allTypes);
				
				fetchData.activities = Activities.all;
			})

		/*
		 * After we have processed the settings, we have all the data we need to
		 * start the actual computation.
		 */
		fetchData.settingsProcessed.then(() => {
			var calculate = notificationData.forPromise('Fetching data for ' + this.request.name, $q.all(fetchData))
				.then(values => {
					/*
					 * Step 1:
					 * Annotate every node in the recipe tree with data that
					 * allows us to decide for each material which of the
					 * recipes (buy or produce) is cheaper.
					 */
					
					// Annotate every recipe node with the activity's cost index
					// We could just look them up later instead of annotating,
					// but it's cleaner this way
					var costIndices = {}
					for(i in values.activities) {
						// There is no buying cost index
						if(i == 0) continue;
						var x = values.activities[i];
						var costIndex = this[x.esiKey + 'costIndex'];
						if(!costIndex && values.settings.productionSystem)
							costIndex = values.settings.productionSystem.costIndices[x.id];
						costIndices[x.id] = costIndex;
					}
					this.traverseRecipes(values.settings.acquisition, {
						beforeRecipe: (recipe, producedMaterial) => {
							if(costIndices[recipe.activityID])
								recipe.costIndex = costIndices[recipe.activityID];
						}
					});
					
					// Calculate how many of each material we need for a
					// production job and how high the installation costs are
					// top-down traversal, we already have requestedAmount on this.request
					// we want requestedAmount on every material.
					this.traverseRecipes(values.settings.acquisition, {
						beforeRecipe: (recipe, producedMaterial) => {
							recipe.activity = values.activities[recipe.activityID];
							if(recipe.activityID == 1) {
								// TODO this will be more complicated for T2/T3
								// TODO we're assuming here that everything is produced from copies.
								// 		that's not always true
								// How many items can we produce with a full BPC
								recipe.producedPerSlot = recipe.maxProductionLimit * recipe.producedPerRun;
								// How many full BPCs do we need
								recipe.fullSlots = Math.floor(producedMaterial.requestedAmount / recipe.producedPerSlot);
								// How many runs do we need on the last (not fully used) BPC
								recipe.partialSlot = (producedMaterial.requestedAmount - (recipe.fullSlots * recipe.producedPerSlot)) / recipe.producedPerRun;
								
								// We need this later to show how many materials
								// are needed to produce one producedMaterial
								recipe.numberProduced = producedMaterial.requestedAmount;
								
								// TODO Following is only true for manufacturing
								recipe.baseJobCost = 0;
								for(i in recipe.materials)
									recipe.baseJobCost += recipe.materials[i].rawAmount * recipe.materials[i].type.adjustedCosts;
								
								// TODO cost index precedence: Should we use
								// request.override > request.system > page.override > page.system
								// instead?
								// TODO account for BP copying costs
								// TODO account for facility job costs modifier
								if(!!recipe.costIndex) {
									recipe.jobFee = -(recipe.baseJobCost * recipe.costIndex
										* (recipe.fullSlots * recipe.maxProductionLimit + recipe.partialSlot));
									recipe.totalInstallationCost = recipe.jobFee;
									if(this.facilityTax) {
										recipe.totalFacilityTax = recipe.jobFee * this.facilityTax / 100;
									} else result.missingSettingWarnings.addInfix('Facility tax');
								} else result.missingSettingWarnings.addInfix('Manufacturing CI or production system');
								
								// TODO account for facility efficiency modifier
								recipe.meModifier = (100 - this.bpID2MeLevel(recipe.mainComponent.type.id)) / 100;
							}
						},
						beforeMaterial: (material, forRecipe) => {
							// Not needed on acquisition root object
							if(forRecipe == null) return;
							var forFullSlots = forRecipe.fullSlots * this.neededMaterials(forRecipe.maxProductionLimit, material.rawAmount, forRecipe.meModifier);
							var forPartialSlot = this.neededMaterials(forRecipe.partialSlot, material.rawAmount, forRecipe.meModifier);
							material.requestedAmount = forFullSlots + forPartialSlot;
						}
					});
					
					/*
					 * Step 2:
					 * Decide for each material node which is the cheapest price
					 * to produce it
					 */
					
					// Bottom-up traversal: For each recipe, choose which is
					// cheaper: Buying or producing it. Every recipe gets a
					// costPerItem property, and every material gets an
					// activeRecipe property with a reference to the cheaper
					// recipe
					this.traverseRecipes(values.settings.acquisition, {
						beforeMaterial: (material, forRecipe) => {
							// TODO hard-coded activityID is ugly
							var buyNode = material.recipes['0'];
							if(buyNode) buyNode.costPerItem = material.type.buyPrice;
						},
						afterRecipe: (recipe, producedMaterial) => {
							if(recipe.activityID == 1) {
								// Can't use map/reduce here, as materials is not an array
								var materialCost = 0;
								for(i in recipe.materials) {
									materialCost += recipe.materials[i].costPerAll;
								}
								recipe.costPerAll = materialCost + recipe.totalInstallationCost;
								recipe.costPerItem = recipe.costPerAll / producedMaterial.requestedAmount;
							}
						},
						afterMaterial: (material, forRecipe) => {
							// Remember costs are already negative, so highest is cheapest
							var currentMin = Number.MIN_SAFE_INTEGER;
							for(i in material.recipes) {
								var x = material.recipes[i];
								if(x.costPerItem > currentMin) {
									material.activeRecipe = x;
									currentMin = x.costPerItem;
								}
							}
							material.costPerItem = currentMin;
							material.costPerAll = currentMin * material.requestedAmount;
							// null on the acquisition root
							if(forRecipe == null)
								material.forOneParent = amt;
							else
								material.forOneParent = material.requestedAmount / forRecipe.numberProduced;
						} 
					});
					
					// For each recipe, collect which
					// materials we need to complete it and what its
					// installation costs are.
					// Note the 'true' at the very end of the parameter list,
					// which means that we only traverse the active recipe for
					// every material node.
					this.traverseRecipes(values.settings.acquisition, {
						afterRecipe: (recipe, producedMaterial) => {
							// TODO this can be generalized among activities and cost types
							if(recipe.activityID == 0) {
								if(isNaN(producedMaterial.type.buyPrice))
									result.unavailableItemErrors.addInfix(producedMaterial.type.name);
								result.materials.addItemAll(
										producedMaterial.type.id,
										producedMaterial.type,
										producedMaterial.requestedAmount * producedMaterial.type.buyPrice,
										producedMaterial.requestedAmount
									)
							} else if(recipe.activityID == 1) {
								if(recipe.totalInstallationCost)
									result.fees.addItemAll('mcost',
											{ name: 'Manufacture costs', details: $filter('number')(recipe.costIndex, 5) },
											recipe.totalInstallationCost);
								if(recipe.totalFacilityTax)
									result.fees.addItemAll('mtax',
											{ name: 'Manufacture tax', details: this.facilityTax + ' %' },
											recipe.totalFacilityTax);
							}
						}
					}, true)
					
					var salesRev = this.request.type.sellPrice;
					result.sales.addItem('revenue', 'Sales revenue', salesRev);
					
					if(this.brokerFee != null) result.fees.addItem('bfee',
							{ name: 'Broker fee', details: this.brokerFee + ' %' },
							-(this.brokerFee * salesRev / 100));
					else result.missingSettingWarnings.addInfix('Broker fee');
					
					if(this.salesTax != null) result.fees.addItem('stax',
							{ name: 'Sales tax', details: this.salesTax + ' %'},
							-(this.salesTax * salesRev / 100));
					else result.missingSettingWarnings.addInfix('Sales tax');

					result.profit.finalize();
				})
		})
	}
	
	// We don't add the listener to to request repositories here; doing so would
	// not guarantee that the listener is actually called, since we need to
	// dependency inject ProductionCalculator somewhere for this code to be
	// executed. We could just inject ProductionCalculator into some arbitrary
	// factory that gets executed at the beginning, but that isn't future proof
	// as we could remove it later by accident. Rather, we inject in into
	// ProductionCtrl (as that gets executed when the production module is
	// loaded, but doesn't lead to a cyclic dependency) and add the listener
	// there as well.
	Calculator.requestItemListener = {
		onEachInstance: function(request) {
			request.calculator = new Calculator(request);
			request.calculator.update();
		},
		onUpdate: function(request) {
			request.calculator.update();
		}
	};
	
	return Calculator;
})
.directive('expenseGroup', function() {
	return {
		templateUrl: 'js/production/expense-group.html',
		scope: { model: '=' }
	}
})
.directive('expenseItem', function() {
	return {
		templateUrl: 'js/production/expense-item.html',
		scope: { model: '=' }
	}
})
.directive('errorGroup', function() {
	return {
		templateUrl: 'js/production/error-group.html',
		scope: {
			model: '=',
			cls: '='
		}
	}
})
.directive('productionRecipe', function() {
	return {
		templateUrl: 'js/production/production-recipe.html',
		scope: {
			model: '=',
			indent: '=',
			active: '='
		}
	}
})
.directive('productionMaterial', function() {
	return {
		templateUrl: 'js/production/production-material.html',
		scope: {
			model: '=',
			indent: '='
		}
	}
})

.controller('ProductionCtrl', function($scope, $uibModal, Auth, productionData, ProductionPageRepository, ProductionRequestRepository, ProductionCalculator) {
	$scope.authStatus = Auth;
	$scope.data = productionData;
	
	// Can't use accordions to collapse requests because it doesn't work well
	// with links in the headers
	$scope.requestExpanded = {};
	
	// We could call the following straight from the partial, but it's a bit too
	// much logic (especially waiting for the promise) to do so according to my
	// tastes. We do this for delete as well for consistency.
	$scope.update = function(request) {
		request.acquisitionPromise.then(() => productionData.currentPage.requests.update(request));
	}
	$scope.delete = function(request) {
		productionData.currentPage.requests.delete(request);
	}
	
	// If page settings are changed, update the calculators of every request on
	// that page
	// Waiting for login status to be resolved because we suspected a race
	// condition that would lead to the listener not being registered in some
	// (rare) page reloads
	productionData.loginStatus.then(() => {
		if(productionData.pages != null) {
			productionData.pages.addListener({
				onCreate: page => productionData.currentPage = page,
				onDelete: page => { if(productionData.currentPage === page) productionData.currentPage = null },
				onUpdate: page => {
					// Since we're async, the page may or may not have changed while the
					// update request was taking its round trip
					if(productionData.currentPage == page) {
						for(i in page.requests.items) {
							var r = page.requests.items[i];
							// Might be null because there's a promise in items
							if(r.calculator) r.calculator.update();
						}
					}
				}
			});
		}
	});
	
	// For an explanation of why we do this here, see the comment on
	// ProductionCalculator.listener
	ProductionRequestRepository.addListener(ProductionCalculator.requestItemListener);
})

.controller('ProductionSettingsCtrl', function($scope, productionData) {
	// If we show the form for a request, we allow to override the default
	// settings made for the page. Also requests have an associated item type,
	// so we allow users to override ME, TE and skill levels for the individual
	// blueprints and skills.
	var requestMode = $scope.request !== undefined;
	$scope.requestMode = requestMode;
	$scope.pageMode = !requestMode;
	
	$scope.item = requestMode ? $scope.request.settings : $scope.page.settings;
	
	// If we're in request mode, we need to fetch the current page so we
	// know the default settings for that page. We can simply fetch that
	// from productionData -- We're in modal in all current usage scenarios,
	// so the page can't be changed without exiting the form anyway.
	if(requestMode) $scope.parent = productionData.currentPage.settings;
})
.directive('productionSettings', function() {
	return {
		restrict: 'E',
		templateUrl: 'js/production/forms/ProductionSettings.html',
		controller: 'ProductionSettingsCtrl',
		scope: {
			page: '=',
			request: '=',
			form: '='
		}
	}
})

/*
 * Allows us to display the form groups for pages and requests with far
 * less repetition. This directive handles displaying the label of the setting
 * and allows overriding the value if applicable. This is done by using
 * transclusion: Users define the element to be shown if the value is overridden
 * inside the override tag and the element to be shown if the value is inherited
 * inside the inherit tag.
 * 
 * TODO Although better than manually repeating the code for both the state
 * where a value will be inherited and the state where it's overridden as well
 * as repeating the code to control overriding, this is still not an elegant
 * solution, since the elements inside the override and inherit tag only differ
 * by the referenced model and whether the element is disabled or not. Ideally,
 * we would only require a single element to be defined, handing it to the
 * controller. Then the controller could change the referenced model and
 * disabled attribute depending on whether the value is being overridden or
 * inherited. This should be possible somehow by providing a link function
 * to the directive, but we didn't get it to work just right as of now.
 */
.controller('FormGroupCtrl', function($scope) {
	$scope.allowOverride = !!$scope.parent;
	$scope.override = $scope.allowOverride &&
			$scope.target[$scope.property] != null;
	$scope.overrideChanged = function() {
		$scope.override = !$scope.override;
		if($scope.override)
			$scope.target[$scope.property] = $scope.parent[$scope.property];
		else
			$scope.target[$scope.property] = null;
	}
	
	$scope.value = $scope.target[$scope.property];
})
.directive('settingsFormGroup', function() {
	return {
		restrict: 'E',
		templateUrl: 'js/production/forms/form-group.html',
		controller: 'FormGroupCtrl',
		scope: {
			property: '=',
			label: '=',
			target: '=',
			parent: '='
		},
		transclude: {
			override: 'override',
			inherit: '?inherit'
		}
	}
})