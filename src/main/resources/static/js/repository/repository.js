/*
 * Provides a glue layer between the notification module and angular's
 * resource module to reduce boilerplate.
 * 
 * For a $resource, provides some additional functions like showing modal
 * dialogs to create, edit and delete items (so that the user only has to define
 * a template of a form for editing an item). Shows notifications while items
 * are fetched, created updated or deleted. Users can provide callbacks that are
 * invoked whenever items are changed.
 * 
 * For displaying names of items in the UI, we rely on items having a 'name'
 * property.
 */


angular.module('repository', [ 'notification', 'ngResource', 'ui.bootstrap' ])

.factory('Repository', function(notificationData, $uibModal, $q) {
	/*
	 * resource: ngResource function to access the REST backend. Items in the
	 * 		resource are assumed to have id and name properties.
	 * metaData: Object whose properties describe how the items are to be
	 * 		described and managed. Supported properties:
	 * 		- label{Uc,Lc}: how items in the resource are named in upper-
	 * 			and lower case
	 * 		- labelPl{Uc,Lc}: same as above, but plural
	 * 		- templateUrl: URL to a template of a form to create / edit items
	 * 		- transientProperties: Property names that will be ignored when
	 * 			sending an update request to the server.
	 * 		- defaultProperties: Properties to set for a newly created item
	 * 			before the 'create item' modal is shown. This will be used if
	 * 			no parameter is passed to the create function of the repository.
	 */
	function Repository(resource, metaData) {
		this.resource = resource;
		this.metaData = metaData;
		this.listeners = [];
		
		this.items = null;
		
		this.query();
	};
	
	Repository.prototype = {}
	
	/*
	 * Adds a listener that will be called whenever an operation completed on an
	 * item successfully. Currently, a listener can define the functions
	 * onCreate, onUpdate, onDelete and onEachInstance. Each of this functions
	 * can take an item as parameter.
	 * The on{Create,Update,Delete} functions are called if an item has been
	 * created, updated or deleted, respectively. These functions are only
	 * called if we received a positive response from the server, i.e. the
	 * action has been performed and persisted.
	 * The onEachInstance function will be called exactly once for each item
	 * instance in this repository. This means that it will be called once for
	 * every item already in the repository when the listener is added, and then
	 * called for every item that has been created. The listener is explicitly
	 * not called when an item is updated, as the local instance does not change
	 * in that case; only its properties are updated. The purpose of this
	 * function is to patch properties into item instances while decoupling from
	 * the specific type of the item. This is needed to avoid cyclic
	 * dependencies between item types.
	 */
	Repository.prototype.addListener = function(listener) {
		this.listeners.push(listener);
		if(this.items) {
			this.items.forEach(this.notifyListener('EachInstance').bind(this));
		}
	}
	
	/*
	 * Shows a modal with the specified template and controller. Returns a
	 * promise that resolves to the result of the modal and never rejects (if
	 * the user dismisses the modal, the error is swallowed so the caller can
	 * focus on showing networking errors further down the promise chain.
	 * 
	 * @param mode Name of the template and controller to use
	 * 		(Create/Update/Delete)
	 * @param item The whose data should be shown / edited by the modal
	 */
	Repository.prototype.showModal = function(action, item) {
		return $uibModal.open({
			animation: true,
			templateUrl: 'js/repository/modals/' + action + '.html',
			controller: 'Item' + action + 'Ctrl',
			controllerAs: '$ctrl',
			resolve: { item: item, metaData: this.metaData }
		}).result.catch(() => $q(() => null))
	}
	
	/*
	 * Shortcut for registering a promise to the notification module, indicating
	 * to the user that an action is currently taking place via XHR. The
	 * notification module then notifies the user if that action failed or
	 * succeeded.
	 * 
	 * @param name Name of the action to show to the user (Create/Update/Delete)
	 * @return A lambda function that takes one parameter, the item on which the
	 * 		action is performed. This is needed to show the item's name in the
	 * 		notification and to get its $promise property, which indicates the
	 * 		result of the XHR. The lambda function then returns a promise chained
	 * 		to item.$promise, mirroring its results
	 */
	Repository.prototype.showNotification = function(action) {
		return item => notificationData.forPromise(action + ' ' + this.metaData.labelLc + ' ' + item.name, item.$promise);
	}
	
	/*
	 * Helper function that notifies listeners that an action has been performed
	 * on an item in this repository successfully and the XHR returned. Actions
	 * are Create/Update/Delete. This object allows one listener per action. The
	 * listener can be registered at creation time by passing a listener
	 * parameter to the constructor that has one or multiple of 'onCreate',
	 * 'onUpdate' or 'onDelete' as functions. The listener functions will be
	 * passed the item on which an action has been performed as parameter.
	 * 
	 * @param action Name of the action to show to the user (Create/Update/Delete)
	 * @return A lambda function that take one parameter, the item on which the
	 * 		action has been performed. This is then passed to the correct listener
	 * 		function, if any exists. To support promise chaining, the item is
	 * 		then returned by the lambda function.
	 */
	Repository.prototype.notifyListener = function(action) {
		var event = 'on' + action;
		return item => {
			for(i in this.listeners) {
				var listener = this.listeners[i];
				if(listener[event]) listener[event](item);
			}
			return item;
		}
	}
	
	/*
	 * Helper function that queries all items that are in this repository from
	 * the server. It is called by the constructor and needs not be called at
	 * any other time if this repository is used correctly, unless there might
	 * be inconsistencies by multiple clients logged in that can write the same
	 * items on the server.
	 */
	Repository.prototype.query = function() {
		this.items = this.resource.query();
		var notify = this.notifyListener('EachInstance').bind(this);
		notificationData.forPromise('Fetching ' + this.metaData.labelPlLc, this.items.$promise)
			.then(xs => xs.forEach(notify));
	}
	
	/*
	 * Helper function that removes all properties from an object that are in
	 * this.metaData.transientProperties. This is used to ensure that no
	 * unnecessary data is sent to the server on update requests.
	 * 
	 * This function should NOT be used on items directly, since it will (duh)
	 * remove the properties from the item. The update system uses a temporary
	 * copy of the item so that an update can be rolled back if the user aborts
	 * it or the server reports an error, so this function works there.
	 * 
	 * @param values The object from which to remove properties
	 * @return the same reference as passed as parameter 'values' to support
	 * 		using this function in promise chaining
	 */
	Repository.prototype.removeTransientProperties = function(values) {
		for(key in values) if(this.metaData.transientProperties.includes(key)) delete values[key];
		return values;
	}
	
	/*
	 * Shows a modal allowing the user to create a new item in this repository.
	 * The modal shows a form with which the user can adjust the new item's
	 * properties before sending a creation request to the server. The default
	 * properties for the new item can be passed as argument.
	 * If the user dismisses the creation modal, then nothing is done. If the
	 * user closes the modal using the 'OK' button, then a creation request is
	 * sent to the server. Once the item has been created server-side, it is
	 * added to this repository and the 'onCreate' listener is called.
	 * 
	 * @param defaultProps Properties of the created item, can be changed by the
	 * 		user in the modal before the item creation request is sent to the
	 * 		server
	 */
	Repository.prototype.create = function(defaultProps) {
		// This _looks_ like it would add the item even though it $promise has
		// not yet returned. That's not true, however, since showNotification
		// returns a promise that is chained to the created item's promise.
		if(defaultProps === undefined) defaultProps = this.metaData.defaultProperties
		this.showModal('Create', new this.resource(defaultProps))
			.then(this.resource.save.bind(this))
			.then(this.showNotification('Creating').bind(this))
			.then(item => { this.items.push(item); return item })
			.then(this.notifyListener('EachInstance').bind(this))
			.then(this.notifyListener('Create').bind(this))
	}
	
	/*
	 * Shows a modal allowing the user to update an item's properties. If the
	 * user dismisses the modal, nothing is done. If the user closes the modal
	 * by clicking its 'OK' button, then an update request is sent to the
	 * server.
	 * Once the update request returns, the local object is updated with the new
	 * property values sent by the server. Then the 'onUpdate' listener is
	 * called.
	 * 
	 * @param item The item to update. Its properties won't be changed until
	 * 		they have been changed on the server
	 */
	Repository.prototype.update = function(item) {
		this.showModal('Update', item)
			.then(this.removeTransientProperties.bind(this))
			.then(values => {
				var updateDonePromise = $q((resolve, reject) =>
					this.resource.update({}, values, () => resolve(values), reject))
				// Promise chain at this level, or else the notification is
				// only shown once the request actually returns 
				return this.showNotification('Updating')({ name: item.name, $promise: updateDonePromise })
			})
			.then(values => {
				// We edited the item out-of-place, so now that the XHR returned
				// successfully, we can write the new property values to item
				for(key in values) item[key] = values[key];
				this.notifyListener('Update')(item);
			})
	}
	
	/*
	 * Shows a modal requesting the user to confirm the deletion of an item. If
	 * the user dismisses the modal, nothing is done. If the user closes the
	 * modal, by clicking its 'OK' button, then a deletion request is sent to
	 * the server for the item.
	 * Once the request returns successfully, the local object is deleted from
	 * this repository. Then, the 'onDelete' listener is called.
	 * 
	 * @param item The item to delete. The item won't be deleted locally until
	 * 		the server confirms that it has been removed from the database.
	 */
	Repository.prototype.delete = function(item) {
		// No need to remove transitive properties here since only the ID is
		// sent anyway
		this.showModal('Delete', item)
			.then(() => {
				var deleteDonePromise = $q((resolve, reject) =>
					item.$delete(resolve, reject))
				// Promise chain at this level, or else the notification is
				// only shown once the request actually returns
				return this.showNotification('Deleting')({ name: item.name, $promise: deleteDonePromise })
			})
			.then(() => {
				this.items = this.items.filter(x => x !== item)
				this.notifyListener('Delete')(item)
			})
	}
	
	return Repository
})

.controller('ItemCreateCtrl', function($uibModalInstance, item, metaData) {
	$ctrl = this
	$ctrl.item = item
	$ctrl.metaData = metaData
	$ctrl.ok = function() {
		$uibModalInstance.close(item)
	}
	$ctrl.cancel = function() {
		$uibModalInstance.dismiss()
	}
})

.controller('ItemUpdateCtrl', function($uibModalInstance, item, metaData) {
	var $ctrl = this
	// Don't edit the item model directly, or else changes will be instantly
	// propagated to the model, even if the user clicks cancel afterwards.
	// Instead, we create a new object, copy all the properties of the item to
	// the new object, then copy the properties back from the new object to the
	// item once the user clicked 'OK' and the server persisted the changes
	// successfully
	$ctrl.item = {}
	$ctrl.metaData = metaData
	angular.copy(item, $ctrl.item);
	$ctrl.actualItem = item
	$ctrl.ok = function() {
		$uibModalInstance.close($ctrl.item)
	}
	$ctrl.cancel = function() {
		$uibModalInstance.dismiss()
	}
})

.controller('ItemDeleteCtrl', function($uibModalInstance, item, metaData) {
	var $ctrl = this
	$ctrl.item = item
	$ctrl.metaData = metaData
	$ctrl.ok = function() {
		$uibModalInstance.close()
	}
	$ctrl.cancel = function() {
		$uibModalInstance.dismiss()
	}
})