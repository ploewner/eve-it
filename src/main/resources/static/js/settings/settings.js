angular.module('settings', [])

.factory('settingsData', function() {
	var data = {
		sections: []
	}
	data.addSection = function(label, container, templateUrl) {
		data.sections.push({ label: label, container: container, templateUrl: templateUrl })
	}
	return data
})

.controller('SettingsCtrl', function($scope, settingsData) {
	$scope.sections = settingsData.sections
})