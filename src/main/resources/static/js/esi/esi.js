/*
 * Provides the glue and boilerplate to access CCP's ESI server so we can
 * query it for non-static game data.
 * 
 * Also loads some static data that we extracted from the database from our own
 * server.
 */
angular.module('esi', [ 'notification' ])

.factory('Decryptors', function($http, notificationData) {
	var decryptors = {};
	decryptors.all = notificationData.forPromise('Loading Decryptor data', $http({
		method: 'GET',
		url: 'data/decryptor-data.json'
	})).then(response => response.data);
	
	return decryptors;
})

.factory('Activities', function($http, notificationData) {
	var activities = {};
	activities.all = notificationData.forPromise('Loading Activity data', $http({
		method: 'GET',
		url: 'data/activities.json'
	})).then(response => response.data);
	
	activities.byId = function(id) {
		return activities.all.then(xs => xs[id]);
	}
	activities.byName = function(name) {
		return activities.all.then(xs => {
			for(i in xs) if(xs[i].name === name) return xs[i];
			return null;
		});
	}
	// For bulk calls of byEsiKey where we don't want to wait on every future
	// returned by it, users can wait for activities.all manually, then call
	// this
	activities.readyByEsiKey = function(xs, esiKey) {
		for(i in xs) if(xs[i].esiKey === esiKey) return xs[i];
		return null;
	}
	activities.byEsiKey = function(esiKey) {
		activities.all.then(xs => activities.readyByEsiKey(xs, esiKey));
	}
	
	return activities;
})

.factory('AdjustedCosts', function($http, notificationData) {
	var costs = {};
	// It's a large-ish amount of data, so processing it can take a short while.
	// Thus we use two messages to better inform the users of progress
	var loading = notificationData.forPromise('Loading adjusted costs', $http({
		method: 'GET',
		url: 'https://esi.tech.ccp.is/latest/markets/prices/'
	}));
	costs.all = notificationData.forPromise('Processing adjusted costs', loading)
		.then(response => {
			var data = {};
			response.data.forEach(typeData => data[typeData.type_id] = typeData.adjusted_price);
			return data;
		});
	
	costs.byId = function(id) {
		return costs.all.then(xs => xs[id]);
	}
	costs.byType = function(type) {
		return costs.byId(type.id);
	}
	costs.annotateTypesAs = function(types, propName) {
		for(i in types) delete types[i][propName];
		return costs.all.then(xs => {
			// Using for syntax here so the function can be used with arrays and
			// container objects
			for(i in types) types[i][propName] = xs[types[i].id];
		});
	}
	costs.annotateTypes = function(types) {
		return costs.annotateTypesAs(types, 'adjustedCosts');
	}
	
	return costs;
})

.factory('CostIndices', function($http, notificationData, Activities, $q) {
	var indices = {};
	// It's a large-ish amount of data, so processing it can take a short while.
	// Thus we use two messages to better inform the users of progress
	var loading = {};
	loading.indices = notificationData.forPromise('Loading system cost indices', $http({
		method: 'GET',
		url: 'https://esi.tech.ccp.is/latest/industry/systems/'
	}));
	// Directly synchronize to activities, or else we'd have to wait on
	// thousands of promises for each byEsiKey call later
	loading.activities = Activities.all;
	var loadingAll = $q.all(loading);
	
	indices.all = notificationData.forPromise('Processing system cost indices', loadingAll)
		.then(x => {
			var data = {};
			x.indices.data.forEach(systemData => {
				data[systemData.solar_system_id] = {};
				systemData.cost_indices.forEach(indexData => {
					var activity = Activities.readyByEsiKey(x.activities, indexData.activity);
					// Could be null if we're not using the activity
					if(activity != null)
						data[systemData.solar_system_id][activity.id] = indexData.cost_index;
				});
			});
			return data;
		});
	
	indices.getById = function(systemId, activityId) {
		return indices.all.then(xs => xs[systemId][activityId]);
	}
	indices.getByRef = function(system, activity) {
		return indices.getById(system.id, activity.id);
	}
	indices.annotateSystemsAs = function(systems, propName) {
		for(i in systems) delete systems[i][propName];
		return indices.all.then(xs => {
			for(i in systems) systems[i][propName] = xs[systems[i].id];
		});
	}
	indices.annotateSystems = function(systems) {
		return indices.annotateSystemsAs(systems, 'costIndices');
	}
	
	return indices;
})

.factory('MarketData', function($http, notificationData, $q) {
	var marketData = {};
	
	// Normally we'd cache market data because we generate a lot of requests,
	// but the browser already does that for us (ESI market interface has five
	// minutes of cache time). If we ever change to eve-central, then we do need
	// to cache the market data.
	// TODO we might migrate this to eve-central instead of ESI, because
	// eve-central has a much better API; it support system-wide requests and
	// bulk requests, both of which will lead to a massive increase in
	// performance.
	// TODO currently we're fetching market prices by region, but we'd like them
	// by system
	
	// Shouldn't be called since it won't show a notification, so we made it
	// private
	var fetchPrice = function(type, system, orderType, comparator) {
		return $http({
			method: 'GET',
			url: 'https://esi.tech.ccp.is/latest/markets/' + system.regionId +
				'/orders/?order_type=' + orderType +
				'&type_id=' + type.id
		}).then(response => response.data.length != 0
				? response.data.map(x => x.price).reduce((a,v) => comparator(a, v) ? a : v)
				: NaN
		);
	}
	
	marketData.annotateTypesAs = function(types, system, orderType, comparator, propName) {
		for(i in types) delete types[i][propName];
		return notificationData.forPromise('Fetching ' + orderType + ' order data',$q.all(
			types.map(x => fetchPrice(x, system, orderType, comparator).then(y => {
				x[propName] = y;
			}))
		));
	}
	marketData.annotateTypes = function(types, system, orderType, comparator) {
		return marketData.annotateTypesAs(types, system, orderType, comparator, orderType + 'Price');
	}
	marketData.annotateBuyTypes = function(types, system) {
		// We want instant buy prices, which are equal to sell prices
		return marketData.annotateTypesAs(types, system, 'sell', (x,y) => x < y, 'buyPrice')
			.then(() => { for(i in types) types[i].buyPrice *= -1 });
	}
	marketData.annotateSellTypes = function(types, system) {
		return marketData.annotateTypes(types, system, 'sell', (x,y) => x < y);
	}
	
	
	return marketData;
})