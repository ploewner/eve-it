angular.module('eveit', [ 'util', 'auth', 'home', 'navigation', 'notification', 'production', 'repository', 'settings' ])

.config(function(uiSelectConfig) {
	uiSelectConfig.theme = 'bootstrap';
});

angular.element(document).ready(function($http) {
	initKeycloak().success(function() {
		angular.bootstrap(document, ['eveit']);
	}).error(function() {
		console.log('error loading keycloak');
	});
})