#!/bin/sh

wget https://www.fuzzwork.co.uk/dump/sqlite-latest.sqlite.bz2 &&
wget https://www.fuzzwork.co.uk/dump/sqlite-latest.sqlite.bz2.md5 &&
md5sum -c sqlite-latest.sqlite.bz2.md5 &&
bunzip2 sqlite-latest.sqlite.bz2 &&
mv sqlite-latest.sqlite static-data.db

rm sqlite-latest.sqlite.bz2.md5
