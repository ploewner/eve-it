This is the EVE industry toolkit, an open source, web-based industry calculator
for the computer game eve online. We provide this source code under the revised
BSD license - please see license.txt.

Our end goal is to provide an SPA-based way to calculate which of the possible
production endeavors is currently the most profitable and to make it easier to
acquire the resources needed to complete it.

There's already better tools out there that do the same thing and more, like
fuzzwork and eve-mogul, and they are actually done and do what they should.
This project should be seen less of an attempt to compete with them, and more
of a sample project because i do need a problem to solve while learning the
Spring-boot / JPA / angular stack. It's published because maybe it will help
somebody, somewhere, sometime.

This project is far from done. While it's already possible to configure (and
persist) a lot of stuff, it doesn't use that configuration to actually calculate
anything.

This project requires a keycloak SSO server for user management.

Some notes on the technologies used while we're not ready to deploy this:

- Currently, we're using sqlite3 for the game database. This is only for testing
purposes. The database is not included in this repository, but an sqlite version
of the game's item database is available on CCP's API and community website. It
can also be downloaded by using a shell script that is provided in this
repository (see further down to see how).
- We're using a H2 in-memory database for user data, also only for testing
purposes.
- We assume that both keycloak and the spring server run on the same machine,
localhost and are only accessed from localhost. There is a way to configure
the project to not do this, but as long as it's only a single-person project
without a proper development and test server, there's not much use in doing so
before it's ready to be deployed.

Some notes on configuration / before first run:

- Ensure that your IDE / compiler is configured to support project lombok
annotations

- Create the file "local.properties" in the project's root directory. This file
is not tracked by git and contains variables that are injected in various other
configuration files by maven. This file should contain the following properties:

keycloak.server.url=XXX
keycloak.server.secret=XXX
keycloak.server.public_key=XXX

tools.marketmenu.url=data/producable-menu.json

The property values 'XXX' should be obtained from the keycloak server's config
page. The keycloak server should be configured to use realm
"EvE-IT" with resources "frontend" (for the SPA) and "backend" (for the server).
Note that keycloak.server.url can read something different from, say,
"http://localhost:8000". However, this only influences the keycloak server that
the client tries to access. The backend will still try to access a keycloak
server located at localhost, no matter to what you set this.

- If you plan on running directly from eclipse, make sure to add
"target/filtered-resources" as source directory. The problem with this is that
the tomcat server will always server the second-to-latest resource files...

- Ensure that the static data export SQLite database is downloaded and located
in "test-db/static-data.db". This file is not included in the repository and
must be downloaded manually. An SQLite version of the static data export is
available on CCP's community website.
You can download the latest data dump from fuzzwork by changing into the test-db
directory and running "sh fetch-db.sh"
Every time you download a new database, you need to run the StaticDataExporter
tool from the root directory to export some data from the newly downloaded
database. The data will be saved as json file so it can be directly served.